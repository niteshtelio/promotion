package com.telio.promotion.common.enums;

public enum DiscountType {
    FIXED,
    PERCENTAGE,
    FREE;
}