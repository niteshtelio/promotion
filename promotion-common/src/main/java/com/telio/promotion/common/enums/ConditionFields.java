package com.telio.promotion.common.enums;

public enum ConditionFields {
    BRAND("Brand"),
    SKU_ID("Sku ids"),
    CATEGORY("Category"),
    SUB_CATEGORY("Sub category");

    private final String value;

    ConditionFields(String value) {
        this.value = value;
    }

    public String getValue() {
        return this.value;
    }
}