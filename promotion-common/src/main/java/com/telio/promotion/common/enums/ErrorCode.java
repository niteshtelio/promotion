package com.telio.promotion.common.enums;

public enum ErrorCode {

    INTERNAL_SERVER_ERROR,
    NOT_FOUND,
    BAD_REQUEST;

    @Override
    public String toString() {
        return this.name();
    }
}
