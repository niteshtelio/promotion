package com.telio.promotion.common.enums;

public enum CustomerBound {

    INCLUDE,
    EXCLUDE;
}
