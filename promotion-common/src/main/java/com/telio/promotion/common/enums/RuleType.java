package com.telio.promotion.common.enums;

public enum RuleType {

    GENERAL,
    EXCLUSIVE,
    KNOCKOUT;
}
