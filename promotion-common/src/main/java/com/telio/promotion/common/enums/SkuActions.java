package com.telio.promotion.common.enums;

public enum SkuActions {
    ADD,
    DELETE,
    UPDATE;

    @Override
    public String toString() {
        return this.name();
    }
}
