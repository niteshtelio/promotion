package com.telio.promotion.common.enums;

public enum FieldOperators {
    IN("In"),
    EQUAL("Equal"),
    NOT_IN("Not in");

    private final String value;

    FieldOperators(String value) {
        this.value = value;
    }

    public String getValue() {
        return this.value;
    }
}