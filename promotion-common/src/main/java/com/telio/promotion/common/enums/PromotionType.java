package com.telio.promotion.common.enums;

public enum PromotionType {
    SKU,
    ORDER,
    FREE;

    @Override
    public String toString() {
        return this.name();
    }
}
