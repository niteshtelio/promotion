package com.telio.promotion.common.enums;

public enum  PredicateOperators {

    EQUAL("Equal"),
    LESS_THAN("Less than"),
    GREATER_THAN("Greater than"),
    LESS_THAN_EQUAL("Less than equal"),
    GREATER_THAN_EQUAL("Greater than equal");

    private final String value;

    PredicateOperators(String value) {
        this.value = value;
    }

    public String getValue() {
        return this.value;
    }
}
