package com.telio.promotion.common.enums;

public enum ActionType {

    ORDER_DISCOUNT,
    SKU_DISCOUNT,
    FREE_SKU;
}
