package com.telio.promotion.common.enums;

public enum GroupOperators {
    OR("Or"),
    AND("And");

    private final String value;

    GroupOperators(String value) {
        this.value = value;
    }

    public String getValue() {
        return this.value;
    }
}
