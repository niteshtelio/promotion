package com.telio.promotion.common.exceptions;

public class BadRequestException extends RuntimeException {

    public BadRequestException() {
        //default constructor
    }

    public BadRequestException(String message) {
        super(message);
    }
}
