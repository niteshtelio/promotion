package com.telio.promotion.common.utils;

import com.telio.promotion.common.enums.GroupOperators;
import com.telio.promotion.common.enums.Operators;

import java.util.Collections;
import java.util.EnumMap;
import java.util.Map;

public class OperatorUtils {

    private OperatorUtils() {
        //private constructor
    }
    public static final Map<Operators, String> OPERATORS_STRING_MAP;
    static {
        Map<Operators, String> tempMap = new EnumMap<>(Operators.class);
        tempMap.put(Operators.EQUAL,"==");
        tempMap.put(Operators.GREATER_THAN,">");
        tempMap.put(Operators.GREATER_THAN_EQUAL,">=");
        tempMap.put(Operators.LESS_THAN,"<");
        tempMap.put(Operators.LESS_THAN_EQUAL,"<=");
        OPERATORS_STRING_MAP = Collections.unmodifiableMap(tempMap);
    }

    public static final Map<GroupOperators, String> GROUP_OPERATORS_STRING_MAP;
    static {
        Map<GroupOperators, String> tempMap = new EnumMap<>(GroupOperators.class);
        tempMap.put(GroupOperators.AND,"&&");
        tempMap.put(GroupOperators.OR,"||");
        GROUP_OPERATORS_STRING_MAP = Collections.unmodifiableMap(tempMap);
    }
}
