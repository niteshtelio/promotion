package com.telio.promotion.common.exceptions;

public class EntityNotFoundException extends RuntimeException {

    public EntityNotFoundException() {
        //default constructor
    }

    public EntityNotFoundException(String errorMessage) {
        super(errorMessage);
    }
}
