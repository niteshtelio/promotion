package com.telio.promotion.common.enums;

public enum ApprovalStatus {
    PENDING,
    APPROVED,
    DECLINED;
}
