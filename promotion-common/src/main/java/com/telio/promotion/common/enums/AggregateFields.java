package com.telio.promotion.common.enums;

public enum AggregateFields {
    SUB_PRICE("Sub price"),
    SUB_QUANTITY("Sub quantity");

    private final String value;

    AggregateFields(String value) {
        this.value = value;
    }

    public String getValue() {
        return this.value;
    }
}
