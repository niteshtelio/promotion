package com.telio.promotion.common.enums;

public enum  Operators {
    IN("In"),
    EQUAL("Equal"),
    NOT_IN("Not in"),
    LESS_THAN("Less than"),
    GREATER_THAN("Greater than"),
    LESS_THAN_EQUAL("Less than equal"),
    GREATER_THAN_EQUAL("Greater than equal");

    private final String value;

    Operators(String value) {
        this.value = value;
    }

    public String getValue() {
        return this.value;
    }
}