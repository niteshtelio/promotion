package com.telio.promotion.common.constants;

public class MongoConstant {

    // TEMPLATE_ACTION
    public static final String TEMPLATE_ACTION_ID = "id";
    public static final String TEMPLATE_ACTION_ACTION_ID = "actionId";
    public static final String TEMPLATE_ACTION_ACTION_TYPE = "actionType";
    public static final String TEMPLATE_ACTION_DISCOUNT_TYPE = "discountType";
    public static final String TEMPLATE_ACTION_DISCOUNT_PERCENTAGE = "discountPercentage";
    public static final String TEMPLATE_ACTION_DISCOUNT_AMOUNT = "discountAmount";
    public static final String TEMPLATE_ACTION_DISCOUNT_ON_SKUS = "discountOnSkus";
    public static final String TEMPLATE_ACTION_MAXIMUM_QUANTITY_DISCOUNT = "maximumQuantityDiscount";
    public static final String TEMPLATE_ACTION_FREE_SHIPPING = "freeShipping";
    public static final String TEMPLATE_ACTION_UPDATED_AT = "updatedAt";
    public static final String TEMPLATE_ACTION_MODIFIED_BY = "modifiedBy";

    // TEMPLATE_CONDITION
    public static final String TEMPLATE_CONDITION_ID = "id";
    public static final String TEMPLATE_CONDITION_GROUP_BY = "groupBy";
    public static final String TEMPLATE_CONDITION_AGGREGATES = "aggregates";
    public static final String TEMPLATE_CONDITION_UPDATED_AT = "updatedAt";
    public static final String TEMPLATE_CONDITION_MODIFIED_BY = "modifiedBy";

    // PROMOTION AUDIT
    public static final String PROMOTION_AUDIT_ORDER_ID = "orderId";
    public static final String PROMOTION_AUDIT_PROMOTION_ACTIVE = "promotionActive";
    public static final String PROMOTION_AUDIT_ORDER_CANCELLED = "orderCancelled";
    public static final String PROMOTION_AUDIT_UPDATED_AT = "updatedAt";

}
