package com.telio.promotion.app.controllers;

import com.telio.promotion.core.model.mongo.PromotionAuditEvent;
import com.telio.promotion.dtos.audit.PromotionAuditEventRequestDto;
import com.telio.promotion.service.PromotionAuditService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.List;

@RestController
@ApiOperation("CRUD apis to audit Promotion applied with Sale Order")
@RequestMapping("/v1")
@Validated
public class PromotionAuditController {

    @Autowired
    private PromotionAuditService promotionAuditService;

    @ApiOperation("Get audit information for promotion event by order id")
    @GetMapping(value = "/audit/{orderId}")
    public List<PromotionAuditEvent> getAuditPromotionEvent(@PathVariable @NotNull String orderId) {
        return promotionAuditService.getAuditEvent(orderId);
    }

    @ApiOperation("Audit information for promotion event")
    @PostMapping(value = "/audit")
    public void auditPromotionEvent(@Valid @NotNull @RequestBody PromotionAuditEventRequestDto promotionAuditEventRequestDto) {
        promotionAuditService.auditEvent(promotionAuditEventRequestDto);
    }

    @ApiOperation("Audit information for promotion event in case of order edit")
    @PutMapping(value = "/audit")
    public void auditPromotionEventOrderEdit(@Valid @NotNull @RequestBody PromotionAuditEventRequestDto promotionAuditEventRequestDto) {
        promotionAuditService.auditEventInCaseOfOrderEdit(promotionAuditEventRequestDto);
    }

    @ApiOperation("Cancel promotion audit in case of order cancel")
    @DeleteMapping(value = "/audit/{orderId}")
    public void cancelAuditPromotionEventOrderCancel(@PathVariable @NotNull String orderId) {
        promotionAuditService.cancelAuditEvent(orderId);
    }
}
