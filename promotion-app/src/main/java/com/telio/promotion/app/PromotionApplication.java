package com.telio.promotion.app;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@SpringBootApplication
@ComponentScan(basePackages = {"com.telio.promotion"})
@EnableJpaRepositories("com.telio.promotion.core.repository.postgres")
@EnableMongoRepositories("com.telio.promotion.core.repository.mongo")
@EnableSwagger2
@EnableAutoConfiguration
@EntityScan(basePackages = {"com.telio.promotion.core.model.postgres", "com.telio.promotion.core.model.mongo"})
public class PromotionApplication {

	@Value("${swagger.enabled}")
	private boolean isSwaggerEnabled;

	public static void main(String[] args) {
		SpringApplication.run(PromotionApplication.class, args);
	}

	@Bean
	public Docket swaggerSettings() {
		return new Docket(DocumentationType.SWAGGER_2)
				.enable(isSwaggerEnabled)
				.select()
				.apis(RequestHandlerSelectors.basePackage("com.telio.promotion.app"))
				.paths(PathSelectors.any())
				.build();
	}
}