package com.telio.promotion.app.controllers;

import com.telio.promotion.common.enums.ApprovalStatus;
import com.telio.promotion.dtos.*;
import com.telio.promotion.service.PromotionTemplateService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

@RestController
@ApiOperation("CRUD apis for Promotion Template")
@RequestMapping("/v1")
@Validated
public class PromotionTemplateController {

    @Autowired
    private PromotionTemplateService promotionTemplateService;

    @ApiOperation("Get All Promotion Template")
    @GetMapping("/template")
    public Page<TemplateResponseDto> getAllPromotionTemplate(Pageable pageable) {
        return promotionTemplateService.findAll(pageable);
    }

    @ApiOperation("Get Promotion Template by template name")
    @GetMapping("/template/{templateId}")
    public TemplateResponseDto getPromotionTemplate(@Valid @NotNull @PathVariable Long templateId) {
        return promotionTemplateService.find(templateId);
    }

    @ApiOperation("Create Promotion Template")
    @PostMapping("/template")
    public void createPromotionTemplate(@Valid @NotNull @RequestBody TemplateRequestDto templateRequestDto) {
        promotionTemplateService.create(templateRequestDto);
    }

    @ApiOperation("Edit/Update Promotion Template Action")
    @PutMapping("/template/action/{templateId}")
    public void updatePromotionTemplateAction(@Valid @NotNull @PathVariable Long templateId, @Valid @NotNull @RequestBody TemplateActionDto templateActionDto) {
        promotionTemplateService.update(templateId, templateActionDto);
    }

    @ApiOperation("Edit/Update Promotion Template Condition")
    @PutMapping("/template/condition/{templateId}")
    public void updatePromotionTemplateCondition(@Valid @NotNull @PathVariable Long templateId, @Valid @NotNull @RequestBody TemplateConditionDto templateConditionDto) {
        promotionTemplateService.update(templateId, templateConditionDto);
    }

    @ApiOperation("Edit/Update Promotion Template Name")
    @PutMapping("/template/{templateId}")
    public void updatePromotionTemplateName(@Valid @NotNull @PathVariable Long templateId, @Valid @NotNull @RequestParam String templateName) {
        promotionTemplateService.update(templateId, templateName);
    }

    @ApiOperation("Edit/Update Promotion Template Approval Status")
    @PutMapping("/template/approve/{templateId}")
    public void updatePromotionTemplateApprovalStatus(@Valid @NotNull @PathVariable Long templateId, @Valid @NotNull @RequestParam ApprovalStatus approvalStatus) {
        promotionTemplateService.update(templateId, approvalStatus);
    }

    @ApiOperation("Delete Promotion Template")
    @DeleteMapping("/template/{templateId}")
    public void deletePromotionRuleTemplate(@Valid @NotNull @PathVariable Long templateId) {
        promotionTemplateService.delete(templateId);
    }

}
