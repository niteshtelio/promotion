package com.telio.promotion.app.controllers;

import com.telio.promotion.dtos.Product;
import com.telio.promotion.dtos.PromotionsRequestDto;
import com.telio.promotion.dtos.PromotionsResponseDto;
import com.telio.promotion.dtos.RuleRequestDto;
import com.telio.promotion.dtos.RuleResponseDto;
import com.telio.promotion.dtos.RuleUpdateDto;
import com.telio.promotion.dtos.TemplateConditionDto;
import com.telio.promotion.dtos.TemplateRequestDto;
import com.telio.promotion.dtos.TemplateResponseDto;
import com.telio.promotion.dtos.TemplateUpdateDto;
import com.telio.promotion.service.PromotionService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.List;

@RestController
@RequestMapping("/v1")
public class PromotionController {

    @Autowired
    private PromotionService promotionService;

    @ApiOperation(value = "Create a new promotion template")
    @PostMapping(value = "/templates")
    public TemplateResponseDto createPromotionsTemplate(@Valid @NotNull @RequestBody TemplateRequestDto templateRequestDto) {
        return null;
    }

    @ApiOperation(value = "Get a promotion template by id")
    @GetMapping(value = "/templates/{template_id}")
    public TemplateResponseDto getPromotionsTemplateById(@NotNull @PathVariable(value = "template-id")Long templateId) {
        return null;
    }

    @ApiOperation(value = "Update a promotion template")
    @PutMapping(value = "/templates")
    public boolean updatePromotionsTemplateById(@Valid @NotNull @RequestBody TemplateUpdateDto templateUpdateDto) {
        return true;
    }

    @ApiOperation(value = "Get all promotion templates")
    @GetMapping(value = "/promotion_templates")
    public List<TemplateResponseDto> getAllPromotionTemplates() {
        return null;
    }

    @ApiOperation(value = "Create a new template rule")
    @PostMapping(value = "/templates_rule")
    public RuleResponseDto createTemplatesRule(@Valid @NotNull @RequestBody RuleRequestDto ruleRequestDto) {
        return null;
    }

    @ApiOperation(value = "Get a template rule by id")
    @GetMapping(value = "/templates_rule/{rule_id}")
    public RuleResponseDto getTemplatesRuleById(@NotNull @PathVariable(value = "rule-id")Long ruleId) {
        return null;
    }

    @ApiOperation(value = "Update a template rule")
    @PutMapping(value = "/templates_rule")
    public boolean updateTemplatesRule(@Valid @NotNull @RequestBody RuleUpdateDto ruleUpdateDto) {
        return true;
    }

    @ApiOperation(value = "Get all template rules")
    @GetMapping(value = "/template_rules")
    public List<RuleResponseDto> getAllTemplateRules() {
        return null;
    }

    @ApiOperation(value = "Trigger promotions rule engine")
    @PostMapping(value = "/promotions_engine")
    public PromotionsResponseDto triggerPromotionsRuleEngine(@Valid @NotNull @RequestBody PromotionsRequestDto promotionsRequestDto) {
        return null;
    }

    @ApiOperation(value = "Test api")
    @PostMapping(value = "/template_test")
    public void testApi(@Valid @NotNull @RequestBody TemplateConditionDto templateConditionDto) {
        promotionService.test(templateConditionDto);
    }

    @ApiOperation(value = "Test api")
    @PostMapping(value = "/template_test_two/{id}")
    public Boolean testApiTwo(@Valid @NotNull @RequestBody List<Product> productList, @NotNull @PathVariable(value = "id")String id) {
        return promotionService.test(productList, id);
    }
}