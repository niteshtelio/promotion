package com.telio.promotion.service;

import com.telio.promotion.common.enums.ApprovalStatus;
import com.telio.promotion.dtos.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface PromotionTemplateService {

    void create(TemplateRequestDto templateRequestDto);

    void update(Long templateId, TemplateActionDto templateActionDto);

    void update(Long templateId, TemplateConditionDto templateConditionDto);

    void update(Long templateId, String templateName);

    void update(Long templateId, ApprovalStatus approvalStatus);

    Page<TemplateResponseDto> findAll(Pageable pageable);

    TemplateResponseDto find(Long templateId);

    void delete(Long templateId);
}
