package com.telio.promotion.service.mapper;

import com.telio.promotion.core.model.mongo.TemplateActions;
import com.telio.promotion.core.model.mongo.TemplateConditions;
import com.telio.promotion.core.model.postgres.TemplateMaster;
import com.telio.promotion.dtos.TemplateResponseDto;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring")
public interface TemplateResponseDtoMapper {

    @Mapping(source = "templateMaster.modifiedBy", target = "modifiedBy.id")
    @Mapping(source = "templateMaster.approvedBy", target = "approvedBy.id")
    @Mapping(source = "templateMaster.createdAt", target = "createdAt")
    @Mapping(source = "templateMaster.updatedAt", target = "updatedAt")
    @Mapping(target = "templateCondition.aggregates", ignore = true) //ToDo write map function for the aggregate
    TemplateResponseDto toDto(TemplateMaster templateMaster, TemplateActions templateAction, TemplateConditions templateCondition);
}
