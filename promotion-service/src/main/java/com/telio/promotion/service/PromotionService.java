package com.telio.promotion.service;

import com.telio.promotion.dtos.Product;
import com.telio.promotion.dtos.TemplateConditionDto;

import java.util.List;

public interface PromotionService {
    void test(TemplateConditionDto templateConditionDto);
    Boolean test(List<Product> productList, String id);
}
