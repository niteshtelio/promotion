package com.telio.promotion.service.configs;

import com.telio.promotion.dtos.LoggedInUserDto;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Component;

@Component
@Scope(value = "request", proxyMode = ScopedProxyMode.TARGET_CLASS)
public class RequestContext {

    public LoggedInUserDto getLoggedInUser() {
        LoggedInUserDto loggedInUser = new LoggedInUserDto();
        loggedInUser.setId("12e5eccd-d7fe-4fac-86f5-54f15278b16b");
        loggedInUser.setEmail("system@telio.vn");
        loggedInUser.setName("system");
        return loggedInUser;
    }
}