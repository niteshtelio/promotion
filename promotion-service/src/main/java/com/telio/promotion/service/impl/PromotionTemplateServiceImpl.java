package com.telio.promotion.service.impl;

import com.telio.promotion.common.enums.ApprovalStatus;
import com.telio.promotion.common.exceptions.BadRequestException;
import com.telio.promotion.core.model.mongo.TemplateActions;
import com.telio.promotion.core.model.mongo.TemplateConditions;
import com.telio.promotion.core.model.mongo.User;
import com.telio.promotion.core.model.postgres.TemplateMaster;
import com.telio.promotion.core.repository.mongo.TemplateActionsRepository;
import com.telio.promotion.core.repository.mongo.TemplateConditionsRepository;
import com.telio.promotion.core.repository.postgres.TemplateMasterRepository;
import com.telio.promotion.dtos.*;
import com.telio.promotion.service.PromotionTemplateService;
import com.telio.promotion.service.configs.RequestContext;
import com.telio.promotion.service.mapper.TemplateActionMapper;
import com.telio.promotion.service.mapper.TemplateConditionMapper;
import com.telio.promotion.service.mapper.TemplateMasterMapper;
import com.telio.promotion.service.mapper.TemplateResponseDtoMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class PromotionTemplateServiceImpl implements PromotionTemplateService {

    @Autowired
    private RequestContext requestContext;

    @Autowired
    private TemplateMasterRepository templateMasterRepository;

    @Autowired
    private TemplateConditionsRepository templateConditionsRepository;

    @Autowired
    private TemplateActionsRepository templateActionsRepository;

    @Autowired
    private TemplateResponseDtoMapper templateResponseDtoMapper;

    @Autowired
    private TemplateActionMapper templateActionMapper;

    @Autowired
    private TemplateConditionMapper templateConditionMapper;

    @Autowired
    private TemplateMasterMapper templateMasterMapper;

    @Override
    public void create(TemplateRequestDto templateRequestDto) {
        LoggedInUserDto loggedInUser = requestContext.getLoggedInUser();
        User user = new User(loggedInUser.getId(), loggedInUser.getName(), loggedInUser.getEmail());
        validateTemplateRequestDto(templateRequestDto);
        String templateActionId = saveTemplateAction(templateRequestDto, user);
        String templateConditionId = saveTemplateConditions(templateRequestDto, user);
        TemplateMaster templateMaster = templateMasterMapper.toDto(templateRequestDto, user.getId(), templateConditionId, templateActionId, templateRequestDto.getApprovalStatus(), null);
        templateMasterRepository.save(templateMaster);
    }

    @Override
    public void update(Long templateId, TemplateActionDto templateActionDto) {
        LoggedInUserDto loggedInUser = requestContext.getLoggedInUser();
        User user = new User(loggedInUser.getId(), loggedInUser.getName(), loggedInUser.getEmail());
        TemplateMaster templateMaster = getTemplateMaster(templateId);
        templateActionsRepository.updateTemplateActions(templateMaster.getTemplateActionId(), templateActionDto, user);
    }

    @Override
    public void update(Long templateId, TemplateConditionDto templateConditionDto) {
        LoggedInUserDto loggedInUser = requestContext.getLoggedInUser();
        User user = new User(loggedInUser.getId(), loggedInUser.getName(), loggedInUser.getEmail());
        TemplateMaster templateMaster = getTemplateMaster(templateId);
        templateConditionsRepository.updateTemplateConditions(templateMaster.getTemplateConditionId(), templateConditionDto, user);
    }

    @Override
    public void update(Long templateId, String templateName) {
        LoggedInUserDto loggedInUser = requestContext.getLoggedInUser();
        User user = new User(loggedInUser.getId(), loggedInUser.getName(), loggedInUser.getEmail());
        TemplateMaster templateMaster = getTemplateMaster(templateId);
        templateMasterRepository.updateTemplateName(templateMaster.getTemplateId(), user.getId(), templateName);
    }

    @Override
    public void update(Long templateId, ApprovalStatus approvalStatus) {
        LoggedInUserDto loggedInUser = requestContext.getLoggedInUser();
        User user = new User(loggedInUser.getId(), loggedInUser.getName(), loggedInUser.getEmail());
        TemplateMaster templateMaster = getTemplateMaster(templateId);
        templateMasterRepository.updateTemplateApprovalStatus(templateMaster.getTemplateId(), user.getId(), approvalStatus);
    }

    @Override
    public Page<TemplateResponseDto> findAll(Pageable pageable) {
        Page<TemplateMaster> templateMasterList = templateMasterRepository.findAll(pageable);
        return new PageImpl<>(
                templateMasterList.getContent().stream().map(
                        templateMaster -> {
                            TemplateActions templateActions = getTemplateActions(templateMaster.getTemplateActionId());
                            TemplateConditions templateConditions = getTemplateConditions(templateMaster.getTemplateConditionId());
                            return templateResponseDtoMapper.toDto(templateMaster, templateActions, templateConditions);
                        }
                ).collect(Collectors.toList()), pageable, templateMasterList.getTotalElements());
    }

    @Override
    public TemplateResponseDto find(Long templateId) {
        TemplateMaster templateMaster = templateMasterRepository.findByTemplateId(templateId);
        if(Objects.isNull(templateId)) {
            throw new BadRequestException("No template found for template name: "+templateId);
        }
        TemplateActions templateActions = getTemplateActions(templateMaster.getTemplateActionId());
        TemplateConditions templateConditions = getTemplateConditions(templateMaster.getTemplateConditionId());
        return templateResponseDtoMapper.toDto(templateMaster, templateActions, templateConditions);
    }

    @Override
    public void delete(Long templateId) {
        LoggedInUserDto modifiedUser = requestContext.getLoggedInUser();
        templateMasterRepository.softDeleteTemplate(templateId, modifiedUser.getId());
    }

    private String saveTemplateAction(TemplateRequestDto templateRequestDto, User user) {
        TemplateActions templateActions = templateActionMapper.toDto(templateRequestDto.getTemplateAction(), new Date(), new Date(), user);
        templateActionsRepository.save(templateActions);
        return templateActions.getId();
    }

    private String saveTemplateConditions(TemplateRequestDto templateRequestDto, User user) {
        if(Objects.nonNull(templateRequestDto.getTemplateCondition())) {
            TemplateConditions templateConditions = templateConditionMapper.toDto(templateRequestDto.getTemplateCondition(), new Date(), new Date(), user);
            templateConditionsRepository.save(templateConditions);
            return templateConditions.getId();
        }
        return null;
    }

    private TemplateActions getTemplateActions(String templateActionId) {
        Optional<TemplateActions> templateActions = templateActionsRepository.findById(templateActionId);
        if(templateActions.isPresent()) {
            return templateActions.get();
        }
        else {
            throw new BadRequestException("No template action record present for template action id: "+templateActionId);
        }
    }

    private TemplateConditions getTemplateConditions(String templateConditionId) {
        Optional<TemplateConditions> templateConditions = templateConditionsRepository.findById(templateConditionId);
        if(templateConditions.isPresent()) {
            return templateConditions.get();
        }
        else {
            return null;
        }
    }

    private TemplateMaster getTemplateMaster(Long templateId) {
        TemplateMaster templateMaster = templateMasterRepository.findByTemplateId(templateId);
        if(Objects.isNull(templateMaster)) {
            throw new BadRequestException("No template present for template id: "+templateId);
        }
        return templateMaster;
    }

    private boolean validateTemplateRequestDto(TemplateRequestDto templateRequestDto) {
        if(templateRequestDto.getApprovalStatus() != ApprovalStatus.PENDING) {
            throw new BadRequestException("Approval Status for template must be PENDING while creating template but found: "+templateRequestDto.getApprovalStatus());
        }
        return true;
    }
}
