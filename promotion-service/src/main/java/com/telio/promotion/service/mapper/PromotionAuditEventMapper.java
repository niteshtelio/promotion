package com.telio.promotion.service.mapper;

import com.telio.promotion.core.model.mongo.PromotionAuditEvent;
import com.telio.promotion.dtos.audit.PromotionAuditEventRequestDto;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface PromotionAuditEventMapper {

    PromotionAuditEvent toDto(PromotionAuditEventRequestDto promotionAuditEventRequestDto);
}
