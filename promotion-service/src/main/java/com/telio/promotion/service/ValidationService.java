package com.telio.promotion.service;

import com.telio.promotion.common.exceptions.BadRequestException;
import com.telio.promotion.dtos.AggregatesDto;
import com.telio.promotion.dtos.FieldsDto;
import com.telio.promotion.dtos.TemplateConditionDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import java.util.List;

@Component
public class ValidationService {
    private static final Logger logger = LoggerFactory.getLogger(ValidationService.class);

    public void ValidateTemplateCondition(TemplateConditionDto templateConditionsDto) {
        if(templateConditionsDto == null) {
            return;
        }
        List<List<AggregatesDto>> aggregatesDtos = templateConditionsDto.getAggregates();
        if(CollectionUtils.isEmpty(aggregatesDtos)) {
            throw new BadRequestException("Aggregates can not be null or empty");
        }
        if(templateConditionsDto.getGroupBy() == null && aggregatesDtos.size() > 1) {
            throw new BadRequestException("Group by can not be null");
        }
        for(List<AggregatesDto> aggregatesList : aggregatesDtos) {
            validateAggregates(aggregatesList);
        }
    }

    private void validateAggregates(List<AggregatesDto> aggregatesList) {
        if(CollectionUtils.isEmpty(aggregatesList)) return;
        int aggregatesListSize = aggregatesList.size();
        if(aggregatesListSize == 1) {
            if(aggregatesList.get(0).getAggregateGroupBy() != null) {
                throw new BadRequestException("Unnecessary aggregate group by");
            }
            validateFields(aggregatesList.get(0).getFields());
            return;
        } else if(aggregatesListSize > 1) {
            if(aggregatesList.get(aggregatesListSize - 1).getAggregateGroupBy() != null) {
                throw new BadRequestException("Unnecessary aggregate group by");
            }
        }
        for(int size = aggregatesListSize - 2; size >= 0; size--) {
            AggregatesDto aggregatesDto = aggregatesList.get(size);
            if(aggregatesDto.getAggregateGroupBy() == null) {
                throw new BadRequestException("Aggregate group by can not be null");
            }
            validateFields(aggregatesDto.getFields());
        }
    }

    private void validateFields(List<FieldsDto> fieldsList) {
        if(CollectionUtils.isEmpty(fieldsList)) return;
        int fieldsListSize = fieldsList.size();
        for(FieldsDto fieldsDto : fieldsList) {
            if(CollectionUtils.isEmpty(fieldsDto.getSubFields())) {
                continue;
            }
            for (FieldsDto subFieldsDto : fieldsDto.getSubFields()) {
                if (!CollectionUtils.isEmpty(subFieldsDto.getSubFields())) {
                    throw new BadRequestException("Nested sub fields are not supported");
                }
            }
        }
        if(fieldsListSize == 1 && !CollectionUtils.isEmpty(fieldsList.get(0).getSubFields())) {
            if(fieldsList.get(0).getFieldGroupBy() == null) {
                throw new BadRequestException("Field group by can not be null");
            }
            validateFields(fieldsList.get(0).getSubFields());
            return;
        }
        if(fieldsListSize == 1 && fieldsList.get(0).getFieldGroupBy() != null) {
            throw new BadRequestException("Unnecessary field group by");
        } else if(fieldsListSize > 1 && fieldsList.get(fieldsListSize - 1).getFieldGroupBy() != null) {
            throw new BadRequestException("Unnecessary field group by");
        }
        for(int size = fieldsListSize - 2; size >= 0; size--) {
            FieldsDto fieldsDto = fieldsList.get(size);
            if(fieldsDto.getFieldGroupBy() == null) {
                throw new BadRequestException("Field group by can not be null");
            }
            if(!CollectionUtils.isEmpty(fieldsDto.getSubFields())) {
                validateFields(fieldsDto.getSubFields());
            }
        }
    }
}
