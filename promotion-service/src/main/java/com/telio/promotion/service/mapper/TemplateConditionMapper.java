package com.telio.promotion.service.mapper;

import com.telio.promotion.core.model.mongo.TemplateConditions;
import com.telio.promotion.core.model.mongo.User;
import com.telio.promotion.dtos.TemplateConditionDto;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.util.Date;

@Mapper(componentModel = "spring")
public interface TemplateConditionMapper {

    @Mapping(source = "modifiedBy", target = "modifiedBy")
    @Mapping(target = "id", ignore = true)
    @Mapping(target = "aggregates", ignore = true) //ToDo write map function for the aggregate
    TemplateConditions toDto(TemplateConditionDto templateConditionDto, Date createdAt, Date updatedAt, User modifiedBy);
}
