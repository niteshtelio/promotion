package com.telio.promotion.service.impl;

import com.telio.promotion.core.model.mongo.User;
import com.telio.promotion.core.repository.mongo.TemplateActionsRepository;
import com.telio.promotion.core.repository.mongo.TemplateConditionsRepository;
import com.telio.promotion.dtos.LoggedInUserDto;
import com.telio.promotion.dtos.Product;
import com.telio.promotion.dtos.TemplateConditionDto;
import com.telio.promotion.service.PromotionService;
import com.telio.promotion.service.PromotionsRuleEngine;
import com.telio.promotion.service.TemplatesConditionParser;
import com.telio.promotion.service.ValidationService;
import com.telio.promotion.service.configs.RequestContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PromotionServiceImpl implements PromotionService {
    private static final Logger logger = LoggerFactory.getLogger(PromotionServiceImpl.class);

    @Autowired
    private RequestContext requestContext;

    @Autowired
    private TemplateConditionsRepository templateConditionsRepository;

    @Autowired
    private TemplateActionsRepository templateActionsRepository;

    @Autowired
    private TemplatesConditionParser templatesConditionParser;

    @Autowired
    private ValidationService validationService;

    @Autowired
    private PromotionsRuleEngine promotionsRuleEngine;

    @Override
    public void test(TemplateConditionDto templateConditionDto) {
        validationService.ValidateTemplateCondition(templateConditionDto);
        //promotionsRuleEngine.runRules();
        System.out.println();
    }

    private User getLoggedInUser() {
        LoggedInUserDto loggedInUserDto = requestContext.getLoggedInUser();
        User user = new User();
        user.setEmail(loggedInUserDto.getEmail());
        user.setId(loggedInUserDto.getId());
        user.setName(loggedInUserDto.getName());
        return user;
    }

    @Override
    public Boolean test(List<Product> productList, String id) {
        Boolean b = templatesConditionParser.parseTemplateConditionById(id, productList);
        logger.info("Parsed condition: {}", b);
        return b;
    }
}
