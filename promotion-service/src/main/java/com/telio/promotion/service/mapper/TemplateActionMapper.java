package com.telio.promotion.service.mapper;

import com.telio.promotion.core.model.mongo.TemplateActions;
import com.telio.promotion.core.model.mongo.User;
import com.telio.promotion.dtos.TemplateActionDto;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.util.Date;

@Mapper(componentModel = "spring")
public interface TemplateActionMapper {

    @Mapping(target = "id", ignore = true)
    @Mapping(source = "modifiedBy", target = "modifiedBy")
    TemplateActions toDto(TemplateActionDto templateActionDto, Date createdAt, Date updatedAt, User modifiedBy);
}
