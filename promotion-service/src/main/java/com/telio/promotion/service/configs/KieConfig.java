package com.telio.promotion.service.configs;

import org.kie.api.KieServices;
import org.kie.api.builder.KieBuilder;
import org.kie.api.builder.KieFileSystem;
import org.kie.api.builder.KieModule;
import org.kie.api.runtime.KieContainer;
import org.kie.internal.io.ResourceFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class KieConfig {

    private static final String FREE_SKUS_DRL = "FreeSkus.drl";
    private static final String ORDER_DISCOUNT_FIXED_DRL = "OrderDiscountFixed.drl";
    private static final String ORDER_DISCOUNT_PERCENTAGE_DRL = "OrderDiscountPercentage.drl";
    private static final String SKU_DISCOUNT_FIXED_DRL = "SkuDiscountFixed.drl";
    private static final String SKU_DISCOUNT_PERCENTAGE_DRL = "SkuDiscountPercentage.drl";

    @Bean
    public KieContainer kieContainer() {
        KieServices kieServices = KieServices.Factory.get();
        KieFileSystem kieFileSystem = kieServices.newKieFileSystem();
        kieFileSystem.write(ResourceFactory.newClassPathResource(FREE_SKUS_DRL));
        kieFileSystem.write(ResourceFactory.newClassPathResource(ORDER_DISCOUNT_FIXED_DRL));
        kieFileSystem.write(ResourceFactory.newClassPathResource(ORDER_DISCOUNT_PERCENTAGE_DRL));
        kieFileSystem.write(ResourceFactory.newClassPathResource(SKU_DISCOUNT_FIXED_DRL));
        kieFileSystem.write(ResourceFactory.newClassPathResource(SKU_DISCOUNT_PERCENTAGE_DRL));
        KieBuilder kieBuilder = kieServices.newKieBuilder(kieFileSystem);
        kieBuilder.buildAll();
        KieModule kieModule = kieBuilder.getKieModule();
        return kieServices.newKieContainer(kieModule.getReleaseId());
    }
}
