package com.telio.promotion.service;

import com.telio.promotion.dtos.Product;
import org.kie.api.runtime.KieContainer;
import org.kie.api.runtime.KieSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class PromotionsRuleEngine {

    @Autowired
    private KieContainer kieContainer;

    public void runRules() {
        KieSession kieSession = kieContainer.newKieSession();
        Product product = new Product();
        product.setCategoryId(1);
        product.setSkuId("HCM-0980");
        product.setBrandId(11);
        kieSession.insert(product);
        kieSession.fireAllRules();
        kieSession.dispose();
    }
}