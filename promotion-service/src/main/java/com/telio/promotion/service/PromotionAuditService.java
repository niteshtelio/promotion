package com.telio.promotion.service;

import com.telio.promotion.core.model.mongo.PromotionAuditEvent;
import com.telio.promotion.dtos.audit.PromotionAuditEventRequestDto;

import java.util.List;

public interface PromotionAuditService {

    void auditEvent(PromotionAuditEventRequestDto promotionAuditEventDto);

    void auditEventInCaseOfOrderEdit(PromotionAuditEventRequestDto promotionAuditEventDto);

    void cancelAuditEvent(String orderId);

    List<PromotionAuditEvent> getAuditEvent(String orderId);
}
