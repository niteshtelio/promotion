package com.telio.promotion.service;

import com.telio.promotion.common.enums.AggregateFields;
import com.telio.promotion.common.enums.ConditionFields;
import com.telio.promotion.common.enums.GroupOperators;
import com.telio.promotion.common.exceptions.EntityNotFoundException;
import com.telio.promotion.core.model.mongo.Aggregates;
import com.telio.promotion.core.model.mongo.Fields;
import com.telio.promotion.core.model.mongo.TemplateConditions;
import com.telio.promotion.core.repository.mongo.TemplateConditionsRepository;
import com.telio.promotion.dtos.Product;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Component
public class TemplatesConditionParser {
    private static final Logger logger = LoggerFactory.getLogger(TemplatesConditionParser.class);
    private static final String COMMA = ",";

    @Autowired
    private TemplateConditionsRepository templateConditionsRepository;

    private TemplateConditions getTemplateConditionsById(String templateConditionId) {
        Optional<TemplateConditions> templateConditionsOptional = templateConditionsRepository.findById(templateConditionId);
        if(templateConditionsOptional.isPresent()) {
            return templateConditionsOptional.get();
        } else {
            throw new EntityNotFoundException("No document entry found in collection with id: " + templateConditionId);
        }
    }

    public Boolean parseTemplateConditionById(String templateConditionId, List<Product> productList) {
        TemplateConditions templateConditions = getTemplateConditionsById(templateConditionId);
        List<List<Aggregates>> aggregates = templateConditions.getAggregates();
        if (templateConditions.getGroupBy() == null && aggregates.size() == 1) {
            return processAggregateList(aggregates.get(0), productList);
        } else if (templateConditions.getGroupBy() != null && aggregates.size() > 1) {
            Boolean parsedCondition = null;
            List<Boolean> conditionList = new ArrayList<>();
            for (List<Aggregates> aggregatesList : aggregates) {
                Boolean condition = processAggregateList(aggregatesList, productList);
                if (condition != null) {
                    conditionList.add(condition);
                }
            }
            if (CollectionUtils.isEmpty(conditionList)) {
                return false;
            }
            for (int i = 0; i < conditionList.size(); i++) {
                if (i == 0) {
                    parsedCondition = conditionList.get(i);
                    continue;
                }
                if (templateConditions.getGroupBy() == GroupOperators.AND) {
                    parsedCondition = parsedCondition && conditionList.get(i);
                } else if (templateConditions.getGroupBy() == GroupOperators.OR) {
                    parsedCondition = parsedCondition || conditionList.get(i);
                }
            }
            return parsedCondition;
        }
        return false;
    }

    private Boolean processAggregateList(List<Aggregates> aggregatesList, List<Product> productList) {
        Boolean condition = null;
        GroupOperators nextAggregateGroupBy = null;
        for(Aggregates aggregate : aggregatesList) {
            Boolean subCondition = processAggregates(aggregate, productList);
            if(subCondition == null) {
                continue;
            } else if(condition == null) {
                condition = subCondition;
            } else if(nextAggregateGroupBy != null) {
                if(nextAggregateGroupBy == GroupOperators.AND) {
                    condition = condition && subCondition;
                } else if(nextAggregateGroupBy == GroupOperators.OR) {
                    condition = condition || subCondition;
                }
            }
            if(aggregate.getAggregateGroupBy() != null) {
                nextAggregateGroupBy = aggregate.getAggregateGroupBy();
            }
        }
        return condition;
    }

    private Boolean processAggregates(Aggregates aggregates, List<Product> productList) {
        List<Fields> fieldsList = aggregates.getFields();
        Predicate<Product> predicates = null;
        GroupOperators nextFieldGroupBy = null;
        for(Fields field : fieldsList) {
            Predicate<Product> predicate = processFields(field);
            if(predicates == null) {
                predicates = predicate;
            } else if(nextFieldGroupBy != null) {
                if(nextFieldGroupBy == GroupOperators.AND) {
                    predicates = predicates.and(predicate);
                } else if(nextFieldGroupBy == GroupOperators.OR) {
                    predicates = predicates.or(predicate);
                }
            }
            if(field.getFieldGroupBy() != null) {
                nextFieldGroupBy = field.getFieldGroupBy();
            }
            if(!CollectionUtils.isEmpty(field.getSubFields())) {
                Predicate<Product> subPredicates = null;
                List<Fields> subFields = field.getSubFields();
                GroupOperators nextSubFieldGroupBy = null;
                for(Fields subField: subFields) {
                    Predicate<Product> subPredicate = processFields(subField);
                    if(subPredicates == null) {
                        subPredicates = subPredicate;
                    } else if(nextSubFieldGroupBy != null) {
                        if(nextSubFieldGroupBy == GroupOperators.AND) {
                            subPredicates = subPredicates.and(subPredicate);
                        } else if(nextSubFieldGroupBy == GroupOperators.OR) {
                            subPredicates = subPredicates.or(subPredicate);
                        }
                    }
                    if(subField.getFieldGroupBy() != null) {
                        nextSubFieldGroupBy = subField.getFieldGroupBy();
                    }
                }
                if(subPredicates != null && nextFieldGroupBy != null) {
                    if(nextFieldGroupBy == GroupOperators.AND) {
                        predicates = predicates.and(subPredicates);
                    } else if(nextFieldGroupBy == GroupOperators.OR) {
                        predicates = predicates.or(subPredicates);
                    }
                }
            }
        }
        if(predicates == null) {
            return null;
        }
        switch (aggregates.getPredicateOperator()) {
            case EQUAL:
                if(aggregates.getAggregationOn() == AggregateFields.SUB_PRICE) {
                    return productList.stream().filter(predicates).mapToDouble(Product::getSubTotalPrice).sum() == Double.parseDouble(aggregates.getPredicateValue());
                } else if(aggregates.getAggregationOn() == AggregateFields.SUB_QUANTITY) {
                    return productList.stream().filter(predicates).mapToInt(Product::getQuantity).sum() == Integer.parseInt(aggregates.getPredicateValue());
                }
                break;
            case LESS_THAN:
                if(aggregates.getAggregationOn() == AggregateFields.SUB_PRICE) {
                    return productList.stream().filter(predicates).mapToDouble(Product::getSubTotalPrice).sum() < Double.parseDouble(aggregates.getPredicateValue());
                } else if(aggregates.getAggregationOn() == AggregateFields.SUB_QUANTITY) {
                    return productList.stream().filter(predicates).mapToInt(Product::getQuantity).sum() < Integer.parseInt(aggregates.getPredicateValue());
                }
                break;
            case LESS_THAN_EQUAL:
                if(aggregates.getAggregationOn() == AggregateFields.SUB_PRICE) {
                    return productList.stream().filter(predicates).mapToDouble(Product::getSubTotalPrice).sum() <= Double.parseDouble(aggregates.getPredicateValue());
                } else if(aggregates.getAggregationOn() == AggregateFields.SUB_QUANTITY) {
                    return productList.stream().filter(predicates).mapToInt(Product::getQuantity).sum() <= Integer.parseInt(aggregates.getPredicateValue());
                }
                break;
            case GREATER_THAN:
                if(aggregates.getAggregationOn() == AggregateFields.SUB_PRICE) {
                    return productList.stream().filter(predicates).mapToDouble(Product::getSubTotalPrice).sum() > Double.parseDouble(aggregates.getPredicateValue());
                } else if(aggregates.getAggregationOn() == AggregateFields.SUB_QUANTITY) {
                    return productList.stream().filter(predicates).mapToInt(Product::getQuantity).sum() > Integer.parseInt(aggregates.getPredicateValue());
                }
                break;
            case GREATER_THAN_EQUAL:
                if(aggregates.getAggregationOn() == AggregateFields.SUB_PRICE) {
                    return productList.stream().filter(predicates).mapToDouble(Product::getSubTotalPrice).sum() >= Double.parseDouble(aggregates.getPredicateValue());
                } else if(aggregates.getAggregationOn() == AggregateFields.SUB_QUANTITY) {
                    return productList.stream().filter(predicates).mapToInt(Product::getQuantity).sum() >= Integer.parseInt(aggregates.getPredicateValue());
                }
                break;
        }
        return null;
    }


    private Predicate<Product> processFields(Fields fields) {
        Predicate<Product> predicate = null;
        switch (fields.getFieldOperator()) {
            case EQUAL:
                if(fields.getField() == ConditionFields.SKU_ID) {
                    predicate = product -> product.getSkuId().equalsIgnoreCase(fields.getFieldValue());
                } else if(fields.getField() == ConditionFields.BRAND) {
                    predicate = product -> product.getBrandId() == Integer.parseInt(fields.getFieldValue());
                } else if(fields.getField() == ConditionFields.CATEGORY) {
                    predicate = product -> product.getCategoryId() == Integer.parseInt(fields.getFieldValue());
                } else if(fields.getField() == ConditionFields.SUB_CATEGORY) {
                    predicate = product -> product.getSubCategoryId() == Integer.parseInt(fields.getFieldValue());
                }
                break;
            case IN:
                List<String> fieldsValue = Stream.of(fields.getFieldValue().split(COMMA)).map(String::trim).collect(Collectors.toList());
                if(fields.getField() == ConditionFields.SKU_ID) {
                    predicate = product -> fieldsValue.contains(product.getSkuId());
                } else if(fields.getField() == ConditionFields.BRAND) {
                    predicate = product -> fieldsValue.stream().map(Integer::parseInt).collect(Collectors.toList()).contains(product.getBrandId());
                } else if(fields.getField() == ConditionFields.CATEGORY) {
                    predicate = product -> fieldsValue.stream().map(Integer::parseInt).collect(Collectors.toList()).contains(product.getCategoryId());
                } else if(fields.getField() == ConditionFields.SUB_CATEGORY) {
                    predicate = product -> fieldsValue.stream().map(Integer::parseInt).collect(Collectors.toList()).contains(product.getSubCategoryId());
                }
                break;
            case NOT_IN:
                fieldsValue = Stream.of(fields.getFieldValue().split(COMMA)).map(String::trim).collect(Collectors.toList());
                if(fields.getField() == ConditionFields.SKU_ID) {
                    predicate = product -> !fieldsValue.contains(product.getSkuId());
                } else if(fields.getField() == ConditionFields.BRAND) {
                    predicate = product -> !fieldsValue.stream().map(Integer::parseInt).collect(Collectors.toList()).contains(product.getBrandId());
                } else if(fields.getField() == ConditionFields.CATEGORY) {
                    predicate = product -> !fieldsValue.stream().map(Integer::parseInt).collect(Collectors.toList()).contains(product.getCategoryId());
                } else if(fields.getField() == ConditionFields.SUB_CATEGORY) {
                    predicate = product -> !fieldsValue.stream().map(Integer::parseInt).collect(Collectors.toList()).contains(product.getSubCategoryId());
                }
                break;
        }
        return predicate;
    }
}
