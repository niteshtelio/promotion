package com.telio.promotion.service.mapper;

import com.telio.promotion.common.enums.ApprovalStatus;
import com.telio.promotion.core.model.postgres.TemplateMaster;
import com.telio.promotion.dtos.TemplateRequestDto;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface TemplateMasterMapper {

    TemplateMaster toDto(TemplateRequestDto templateRequestDto, String modifiedBy, String templateConditionId, String templateActionId, ApprovalStatus templateApproved, String approvedBy);
}
