package com.telio.promotion.service.impl;

import com.telio.promotion.common.exceptions.BadRequestException;
import com.telio.promotion.core.model.mongo.PromotionAuditEvent;
import com.telio.promotion.core.repository.mongo.PromotionAuditEventRepository;
import com.telio.promotion.dtos.audit.PromotionAuditEventRequestDto;
import com.telio.promotion.service.PromotionAuditService;
import com.telio.promotion.service.mapper.PromotionAuditEventMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.Objects;

@Service
public class PromotionAuditServiceImpl implements PromotionAuditService {

    @Autowired
    private PromotionAuditEventMapper promotionAuditEventMapper;

    @Autowired
    private PromotionAuditEventRepository promotionAuditEventRepository;

    @Override
    public void auditEvent(PromotionAuditEventRequestDto promotionAuditEventRequestDto) {
        validatePromotionForOrder(promotionAuditEventRequestDto.getOrderId());
        createPromotionAudit(promotionAuditEventRequestDto);
    }

    @Override
    public void auditEventInCaseOfOrderEdit(PromotionAuditEventRequestDto promotionAuditEventRequestDto) {
        promotionAuditEventRepository.disablePromotionAuditEventByOrderId(promotionAuditEventRequestDto.getOrderId());
        createPromotionAudit(promotionAuditEventRequestDto);
    }

    @Override
    public void cancelAuditEvent(String orderId) {
        promotionAuditEventRepository.cancelPromotionAuditByOrderId(orderId);
    }

    @Override
    public List<PromotionAuditEvent> getAuditEvent(String orderId) {
        return promotionAuditEventRepository.findByOrderId(orderId);
    }

    private void validatePromotionForOrder(String orderId) {
        PromotionAuditEvent promotionAuditEvent = promotionAuditEventRepository.findByOrderIdAndPromotionActive(orderId, true);
        if(Objects.nonNull(promotionAuditEvent)) {
            throw new BadRequestException("Invalid request as promotion already active for order id: "+orderId);
        }
    }

    private void createPromotionAudit(PromotionAuditEventRequestDto promotionAuditEventRequestDto) {
        PromotionAuditEvent promotionAuditEvent = promotionAuditEventMapper.toDto(promotionAuditEventRequestDto);
        promotionAuditEvent.setUpdatedAt(new Date());
        promotionAuditEventRepository.save(promotionAuditEvent);
    }
}
