package com.telio.promotion.core.model.mongo;

import com.telio.promotion.common.enums.GroupOperators;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;
import java.util.List;

@Document(collection = "template_conditions")
public class TemplateConditions {

    @Id
    private String id;
    private GroupOperators groupBy;
    private List<List<Aggregates>> aggregates;
    private Date createdAt;
    private Date updatedAt;
    private User modifiedBy;

    public TemplateConditions() {
        //Default Constructor
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public GroupOperators getGroupBy() {
        return groupBy;
    }

    public void setGroupBy(GroupOperators groupBy) {
        this.groupBy = groupBy;
    }

    public List<List<Aggregates>> getAggregates() {
        return aggregates;
    }

    public void setAggregates(List<List<Aggregates>> aggregates) {
        this.aggregates = aggregates;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    public User getModifiedBy() {
        return modifiedBy;
    }

    public void setModifiedBy(User modifiedBy) {
        this.modifiedBy = modifiedBy;
    }
}