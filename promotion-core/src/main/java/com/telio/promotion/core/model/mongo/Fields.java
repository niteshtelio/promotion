package com.telio.promotion.core.model.mongo;

import com.telio.promotion.common.enums.ConditionFields;
import com.telio.promotion.common.enums.FieldOperators;
import com.telio.promotion.common.enums.GroupOperators;

import java.util.List;

public class Fields {

    private ConditionFields field;
    private FieldOperators fieldOperator;
    private String fieldValue;
    private GroupOperators fieldGroupBy;
    private List<Fields> subFields;

    public Fields() {
        //Default Constructor
    }

    public Fields(ConditionFields field, FieldOperators fieldOperator, String fieldValue) {
        this.field = field;
        this.fieldOperator = fieldOperator;
        this.fieldValue = fieldValue;
    }

    public Fields(ConditionFields field, FieldOperators fieldOperator, String fieldValue, GroupOperators fieldGroupBy) {
        this.field = field;
        this.fieldOperator = fieldOperator;
        this.fieldValue = fieldValue;
        this.fieldGroupBy = fieldGroupBy;
    }

    public ConditionFields getField() {
        return field;
    }

    public void setField(ConditionFields field) {
        this.field = field;
    }

    public FieldOperators getFieldOperator() {
        return fieldOperator;
    }

    public void setFieldOperator(FieldOperators fieldOperator) {
        this.fieldOperator = fieldOperator;
    }

    public String getFieldValue() {
        return fieldValue;
    }

    public void setFieldValue(String fieldValue) {
        this.fieldValue = fieldValue;
    }

    public GroupOperators getFieldGroupBy() {
        return fieldGroupBy;
    }

    public void setFieldGroupBy(GroupOperators fieldGroupBy) {
        this.fieldGroupBy = fieldGroupBy;
    }

    public List<Fields> getSubFields() {
        return subFields;
    }

    public void setSubFields(List<Fields> subFields) {
        this.subFields = subFields;
    }
}