package com.telio.promotion.core.repository.mongo;

import com.telio.promotion.core.model.mongo.User;
import com.telio.promotion.dtos.TemplateActionDto;

public interface TemplateActionsRepositoryCustom {

    long updateTemplateActions(String id, TemplateActionDto templateActionDto, User user);
}
