package com.telio.promotion.core.repository.mongo;

import com.telio.promotion.core.model.mongo.TemplateConditions;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;
import java.util.Optional;

@Repository
public interface TemplateConditionsRepository extends MongoRepository<TemplateConditions, String>, TemplateConditionsRepositoryCustom {
    Optional<TemplateConditions> findById(String id);
}