package com.telio.promotion.core.model.postgres;

import com.telio.promotion.common.enums.CustomerBound;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Date;

@Entity
@Table(name = "rule_customer_reference")
public class RuleCustomerReference {

    @Id
    @GeneratedValue(
            strategy = GenerationType.AUTO
    )
    @Column(
            name = "rule_customer_reference_id",
            unique = true
    )
    private Long ruleCustomerReferenceId;

    @Column(name = "rule_id")
    private Long ruleId;

    @NotNull
    @Column(name = "customer_id")
    private String customerId;

    @NotNull
    @Column(name = "customer_bound")
    @Enumerated(EnumType.STRING)
    private CustomerBound customerBound;

    @NotNull
    @Column(name = "created_at")
    @CreationTimestamp
    private Date createdAt;

    public RuleCustomerReference() {
        //Default Constructor
    }

    public Long getRuleCustomerReferenceId() {
        return ruleCustomerReferenceId;
    }

    public void setRuleCustomerReferenceId(Long ruleCustomerReferenceId) {
        this.ruleCustomerReferenceId = ruleCustomerReferenceId;
    }

    public Long getRuleId() {
        return ruleId;
    }

    public void setRuleId(Long ruleId) {
        this.ruleId = ruleId;
    }

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public CustomerBound getCustomerBound() {
        return customerBound;
    }

    public void setCustomerBound(CustomerBound customerBound) {
        this.customerBound = customerBound;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }
}
