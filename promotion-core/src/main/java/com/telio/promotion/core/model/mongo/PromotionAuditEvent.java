package com.telio.promotion.core.model.mongo;

import com.telio.promotion.common.enums.DiscountType;
import com.telio.promotion.dtos.Product;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.persistence.Id;
import java.util.Date;
import java.util.List;

@Document(collection = "promotion_audit")
public class PromotionAuditEvent {

    @Id
    private String auditId;

    @Indexed
    private String orderId;

    private List<Product> sku;

    private List<Product> promotionsSku;

    private DiscountType discountType;

    private double discount;

    private long ruleId;

    private Date appliedAt;

    private Date updatedAt;

    private boolean orderCancelled;

    private boolean promotionActive;

    private String promotionCouponCode;

    private String entityAppliedOn;

    private Double cartTotalPrice;

    private String channel;

    public PromotionAuditEvent() {
        //Default Constructor
        this.promotionActive = true;
    }

    public String getAuditId() {
        return auditId;
    }

    public void setAuditId(String auditId) {
        this.auditId = auditId;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public List<Product> getSku() {
        return sku;
    }

    public void setSku(List<Product> sku) {
        this.sku = sku;
    }

    public List<Product> getPromotionsSku() {
        return promotionsSku;
    }

    public void setPromotionsSku(List<Product> promotionsSku) {
        this.promotionsSku = promotionsSku;
    }

    public DiscountType getDiscountType() {
        return discountType;
    }

    public void setDiscountType(DiscountType discountType) {
        this.discountType = discountType;
    }

    public double getDiscount() {
        return discount;
    }

    public void setDiscount(double discount) {
        this.discount = discount;
    }

    public long getRuleId() {
        return ruleId;
    }

    public void setRuleId(long ruleId) {
        this.ruleId = ruleId;
    }

    public Date getAppliedAt() {
        return appliedAt;
    }

    public void setAppliedAt(Date appliedAt) {
        this.appliedAt = appliedAt;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    public boolean isOrderCancelled() {
        return orderCancelled;
    }

    public void setOrderCancelled(boolean orderCancelled) {
        this.orderCancelled = orderCancelled;
    }

    public boolean isPromotionActive() {
        return promotionActive;
    }

    public void setPromotionActive(boolean promotionActive) {
        this.promotionActive = promotionActive;
    }

    public String getPromotionCouponCode() {
        return promotionCouponCode;
    }

    public void setPromotionCouponCode(String promotionCouponCode) {
        this.promotionCouponCode = promotionCouponCode;
    }

    public String getEntityAppliedOn() {
        return entityAppliedOn;
    }

    public void setEntityAppliedOn(String entityAppliedOn) {
        this.entityAppliedOn = entityAppliedOn;
    }

    public Double getCartTotalPrice() {
        return cartTotalPrice;
    }

    public void setCartTotalPrice(Double cartTotalPrice) {
        this.cartTotalPrice = cartTotalPrice;
    }

    public String getChannel() {
        return channel;
    }

    public void setChannel(String channel) {
        this.channel = channel;
    }
}
