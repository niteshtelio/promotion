package com.telio.promotion.core.repository.mongo;

import com.telio.promotion.core.model.mongo.PromotionAuditEvent;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

public interface PromotionAuditEventRepository extends MongoRepository<PromotionAuditEvent, String>, PromotionAuditEventRepositoryCustom {

    PromotionAuditEvent findByOrderIdAndPromotionActive(String orderId, boolean promotionActive);

    List<PromotionAuditEvent> findByOrderId(String orderId);
}
