package com.telio.promotion.core.repository.mongo.impl;

import com.telio.promotion.common.constants.MongoConstant;
import com.telio.promotion.core.model.mongo.TemplateActions;
import com.telio.promotion.core.model.mongo.User;
import com.telio.promotion.core.repository.mongo.TemplateActionsRepositoryCustom;
import com.telio.promotion.dtos.TemplateActionDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;

import java.util.Date;

public class TemplateActionsRepositoryImpl implements TemplateActionsRepositoryCustom {

    @Autowired
    private MongoTemplate mongoTemplate;

    @Override
    public long updateTemplateActions(String id, TemplateActionDto templateActionDto, User user) {
        Query query = new Query();
        query.addCriteria(Criteria.where(MongoConstant.TEMPLATE_ACTION_ID).is(id));

        Update update = getUpdateConstraints(templateActionDto, user);

        return mongoTemplate.updateFirst(query, update, TemplateActions.class).getModifiedCount();
    }

    private Update getUpdateConstraints(TemplateActionDto templateActionDto, User user) {
        Update update = new Update();
        update.set(MongoConstant.TEMPLATE_ACTION_ACTION_ID, templateActionDto.getActionId());
        update.set(MongoConstant.TEMPLATE_ACTION_ACTION_TYPE, templateActionDto.getActionType());
        update.set(MongoConstant.TEMPLATE_ACTION_DISCOUNT_TYPE, templateActionDto.getDiscountType());
        update.set(MongoConstant.TEMPLATE_ACTION_DISCOUNT_PERCENTAGE, templateActionDto.getDiscountPercentage());
        update.set(MongoConstant.TEMPLATE_ACTION_DISCOUNT_AMOUNT, templateActionDto.getDiscountAmount());
        update.set(MongoConstant.TEMPLATE_ACTION_DISCOUNT_ON_SKUS, templateActionDto.getDiscountOnSkus());
        update.set(MongoConstant.TEMPLATE_ACTION_MAXIMUM_QUANTITY_DISCOUNT, templateActionDto.getMaximumQuantityDiscount());
        update.set(MongoConstant.TEMPLATE_ACTION_FREE_SHIPPING, templateActionDto.isFreeShipping());
        update.set(MongoConstant.TEMPLATE_ACTION_UPDATED_AT, new Date());
        update.set(MongoConstant.TEMPLATE_ACTION_MODIFIED_BY, user.getId());
        return update;
    }
}
