package com.telio.promotion.core.model.mongo;

import org.springframework.data.mongodb.core.index.Indexed;

public class User {

    @Indexed
    private String id;
    private String name;
    private String email;

    public User() {
        //Default Constructor
    }

    public User(String id, String name, String email) {
        this.id = id;
        this.name = name;
        this.email = email;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}