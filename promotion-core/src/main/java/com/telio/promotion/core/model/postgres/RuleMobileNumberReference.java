package com.telio.promotion.core.model.postgres;

import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Date;

@Entity
@Table(name = "rule_mobile_number_reference")
public class RuleMobileNumberReference {

    @Id
    @GeneratedValue(
            strategy = GenerationType.AUTO
    )
    @Column(
            name = "rule_mobile_number_reference_id",
            unique = true
    )
    private Long ruleMobileNumberReferenceId;

    @Column(name = "rule_id")
    private Long ruleId;

    @NotNull
    @Column(name = "mobile_number")
    private String mobileNumber;

    @NotNull
    @Column(name = "created_at")
    @CreationTimestamp
    private Date createdAt;

    public RuleMobileNumberReference() {
        //Default Constructor
    }

    public Long getRuleMobileNumberReferenceId() {
        return ruleMobileNumberReferenceId;
    }

    public void setRuleMobileNumberReferenceId(Long ruleMobileNumberReferenceId) {
        this.ruleMobileNumberReferenceId = ruleMobileNumberReferenceId;
    }

    public Long getRuleId() {
        return ruleId;
    }

    public void setRuleId(Long ruleId) {
        this.ruleId = ruleId;
    }

    public String getMobileNumber() {
        return mobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }
}
