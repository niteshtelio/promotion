package com.telio.promotion.core.repository.mongo.impl;

import com.telio.promotion.common.constants.MongoConstant;
import com.telio.promotion.core.model.mongo.TemplateConditions;
import com.telio.promotion.core.model.mongo.User;
import com.telio.promotion.core.repository.mongo.TemplateConditionsRepositoryCustom;
import com.telio.promotion.dtos.TemplateConditionDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;

import java.util.Date;

public class TemplateConditionsRepositoryImpl implements TemplateConditionsRepositoryCustom {

    @Autowired
    private MongoTemplate mongoTemplate;

    @Override
    public long updateTemplateConditions(String templateConditionId, TemplateConditionDto templateConditionDto, User user) {
        Query query = new Query();
        query.addCriteria(Criteria.where(MongoConstant.TEMPLATE_CONDITION_ID).is(templateConditionId));

        Update update = getUpdateConstraints(templateConditionDto, user);

        return mongoTemplate.updateFirst(query, update, TemplateConditions.class).getModifiedCount();
    }

    private Update getUpdateConstraints(TemplateConditionDto templateConditionDto, User user) {
        Update update = new Update();
        update.set(MongoConstant.TEMPLATE_CONDITION_GROUP_BY, templateConditionDto.getGroupBy());
        update.set(MongoConstant.TEMPLATE_CONDITION_AGGREGATES, templateConditionDto.getAggregates());
        update.set(MongoConstant.TEMPLATE_CONDITION_UPDATED_AT, new Date());
        update.set(MongoConstant.TEMPLATE_CONDITION_MODIFIED_BY, user);
        return update;
    }
}
