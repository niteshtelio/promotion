package com.telio.promotion.core.model.mongo;

import com.telio.promotion.common.enums.AggregateFields;
import com.telio.promotion.common.enums.GroupOperators;
import com.telio.promotion.common.enums.PredicateOperators;

import java.util.List;

public class Aggregates {

    private AggregateFields aggregationOn;
    private List<Fields> fields;
    private PredicateOperators predicateOperator;
    private String predicateValue;
    private GroupOperators aggregateGroupBy;

    public Aggregates() {
        //Default Constructor
    }

    public AggregateFields getAggregationOn() {
        return aggregationOn;
    }

    public void setAggregationOn(AggregateFields aggregationOn) {
        this.aggregationOn = aggregationOn;
    }

    public List<Fields> getFields() {
        return fields;
    }

    public void setFields(List<Fields> fields) {
        this.fields = fields;
    }

    public PredicateOperators getPredicateOperator() {
        return predicateOperator;
    }

    public void setPredicateOperator(PredicateOperators predicateOperator) {
        this.predicateOperator = predicateOperator;
    }

    public String getPredicateValue() {
        return predicateValue;
    }

    public void setPredicateValue(String predicateValue) {
        this.predicateValue = predicateValue;
    }

    public GroupOperators getAggregateGroupBy() {
        return aggregateGroupBy;
    }

    public void setAggregateGroupBy(GroupOperators aggregateGroupBy) {
        this.aggregateGroupBy = aggregateGroupBy;
    }
}