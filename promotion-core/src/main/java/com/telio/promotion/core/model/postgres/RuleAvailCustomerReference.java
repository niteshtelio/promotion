package com.telio.promotion.core.model.postgres;

import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Date;

@Entity
@Table(name = "rule_avail_customer_reference")
public class RuleAvailCustomerReference {

    @Id
    @GeneratedValue(
            strategy = GenerationType.AUTO
    )
    @Column(
            name = "rule_avail_customer_reference_id",
            unique = true
    )
    private Long ruleAvailCustomerReferenceId;

    @Column(name = "rule_id")
    private Long ruleId;

    @NotNull
    @Column(name = "customer_id")
    private String customerId;

    @Column(name = "availed_count")
    private Integer availedCount;

    @NotNull
    @Column(name = "availed_date")
    @CreationTimestamp
    private Date availedDate;

    public RuleAvailCustomerReference() {
        //Default Constructor
    }

    public Long getRuleAvailCustomerReferenceId() {
        return ruleAvailCustomerReferenceId;
    }

    public void setRuleAvailCustomerReferenceId(Long ruleAvailCustomerReferenceId) {
        this.ruleAvailCustomerReferenceId = ruleAvailCustomerReferenceId;
    }

    public Long getRuleId() {
        return ruleId;
    }

    public void setRuleId(Long ruleId) {
        this.ruleId = ruleId;
    }

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public Integer getAvailedCount() {
        return availedCount;
    }

    public void setAvailedCount(Integer availedCount) {
        this.availedCount = availedCount;
    }

    public Date getAvailedDate() {
        return availedDate;
    }

    public void setAvailedDate(Date availedDate) {
        this.availedDate = availedDate;
    }
}
