package com.telio.promotion.core.model.mongo;

import com.telio.promotion.common.enums.ActionType;
import com.telio.promotion.common.enums.DiscountType;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;
import java.util.List;

@Document(collection = "template_actions")
public class TemplateActions {

    @Id
    private String id;
    private Integer actionId;
    private ActionType actionType;
    private DiscountType discountType;
    private double discountPercentage;
    private double discountAmount;
    private List<String> discountOnSkus;
    private Integer maximumQuantityDiscount;
    private boolean freeShipping;
    private Date createdAt;
    private Date updatedAt;
    private User modifiedBy;

    public TemplateActions() {
        //Default Constructor
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Integer getActionId() {
        return actionId;
    }

    public void setActionId(Integer actionId) {
        this.actionId = actionId;
    }

    public ActionType getActionType() {
        return actionType;
    }

    public void setActionType(ActionType actionType) {
        this.actionType = actionType;
    }

    public DiscountType getDiscountType() {
        return discountType;
    }

    public void setDiscountType(DiscountType discountType) {
        this.discountType = discountType;
    }

    public double getDiscountPercentage() {
        return discountPercentage;
    }

    public void setDiscountPercentage(double discountPercentage) {
        this.discountPercentage = discountPercentage;
    }

    public double getDiscountAmount() {
        return discountAmount;
    }

    public void setDiscountAmount(double discountAmount) {
        this.discountAmount = discountAmount;
    }

    public List<String> getDiscountOnSkus() {
        return discountOnSkus;
    }

    public void setDiscountOnSkus(List<String> discountOnSkus) {
        this.discountOnSkus = discountOnSkus;
    }

    public Integer getMaximumQuantityDiscount() {
        return maximumQuantityDiscount;
    }

    public void setMaximumQuantityDiscount(Integer maximumQuantityDiscount) {
        this.maximumQuantityDiscount = maximumQuantityDiscount;
    }

    public boolean isFreeShipping() {
        return freeShipping;
    }

    public void setFreeShipping(boolean freeShipping) {
        this.freeShipping = freeShipping;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    public User getModifiedBy() {
        return modifiedBy;
    }

    public void setModifiedBy(User modifiedBy) {
        this.modifiedBy = modifiedBy;
    }
}