package com.telio.promotion.core.model.postgres;

import com.telio.promotion.common.enums.ActionType;
import com.telio.promotion.common.enums.DiscountType;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Date;

@Entity
@Table(name = "template_actions_reference")
public class TemplateActionsReference {

    @Id
    @GeneratedValue(
            strategy = GenerationType.AUTO
    )
    @Column(
            name = "action_id",
            unique = true
    )
    private Long actionId;

    @NotNull
    @Column(name = "action_type")
    @Enumerated(EnumType.STRING)
    private ActionType actionType;

    @NotNull
    @Column(name = "discount_type")
    @Enumerated(EnumType.STRING)
    private DiscountType discountType;

    @NotNull
    @Column(name = "created_at")
    @CreationTimestamp
    private Date createdAt;

    public TemplateActionsReference() {
        //Default Constructor
    }

    public Long getActionId() {
        return actionId;
    }

    public void setActionId(Long actionId) {
        this.actionId = actionId;
    }

    public ActionType getActionType() {
        return actionType;
    }

    public void setActionType(ActionType actionType) {
        this.actionType = actionType;
    }

    public DiscountType getDiscountType() {
        return discountType;
    }

    public void setDiscountType(DiscountType discountType) {
        this.discountType = discountType;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }
}
