package com.telio.promotion.core.repository.postgres;

import com.telio.promotion.core.model.postgres.RuleAvailCustomerReference;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RuleAvailCustomerReferenceRepository extends JpaRepository<RuleAvailCustomerReference, Long> {
}
