package com.telio.promotion.core.repository.postgres;

import com.telio.promotion.common.enums.ApprovalStatus;

public interface TemplateMasterRepositoryCustom {

    long softDeleteTemplate(Long templateId, String modifiedUserId);

    long updateTemplateName(Long templateId, String modifiedUserId, String templateName);

    long updateTemplateApprovalStatus(Long templateId, String modifiedUserId, ApprovalStatus approvalStatus);
}
