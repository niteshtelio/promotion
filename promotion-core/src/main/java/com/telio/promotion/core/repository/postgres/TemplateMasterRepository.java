package com.telio.promotion.core.repository.postgres;

import com.telio.promotion.core.model.postgres.TemplateMaster;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TemplateMasterRepository extends JpaRepository<TemplateMaster, Long>, TemplateMasterRepositoryCustom {

    Page<TemplateMaster> findAll(Pageable pageable);

    TemplateMaster findByTemplateId(Long templateId);
}
