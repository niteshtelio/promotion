package com.telio.promotion.core.repository.mongo;

import com.telio.promotion.core.model.mongo.PromotionAuditEvent;

public interface PromotionAuditEventRepositoryCustom {

    PromotionAuditEvent getPromotionAuditEventByOrderId(String orderId);

    boolean disablePromotionAuditEventByOrderId(String orderId);

    boolean cancelPromotionAuditByOrderId(String orderId);
}
