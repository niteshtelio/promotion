package com.telio.promotion.core.model.postgres;

import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Date;

@Entity
@Table(name = "rule_customer_group_reference")
public class RuleCustomerGroupReference {

    @Id
    @GeneratedValue(
            strategy = GenerationType.AUTO
    )
    @Column(
            name = "rule_customer_group_reference_id",
            unique = true
    )
    private Long ruleCustomerGroupReferenceId;

    @Column(name = "rule_id")
    private Long ruleId;

    @NotNull
    @Column(name = "customer_group")
    private String customerGroup;

    @NotNull
    @Column(name = "created_at")
    @CreationTimestamp
    private Date createdAt;

    public RuleCustomerGroupReference() {
        //Default Constructor
    }

    public Long getRuleCustomerGroupReferenceId() {
        return ruleCustomerGroupReferenceId;
    }

    public void setRuleCustomerGroupReferenceId(Long ruleCustomerGroupReferenceId) {
        this.ruleCustomerGroupReferenceId = ruleCustomerGroupReferenceId;
    }

    public Long getRuleId() {
        return ruleId;
    }

    public void setRuleId(Long ruleId) {
        this.ruleId = ruleId;
    }

    public String getCustomerGroup() {
        return customerGroup;
    }

    public void setCustomerGroup(String customerGroup) {
        this.customerGroup = customerGroup;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }
}


