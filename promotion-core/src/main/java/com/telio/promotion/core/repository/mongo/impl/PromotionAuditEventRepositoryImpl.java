package com.telio.promotion.core.repository.mongo.impl;

import com.telio.promotion.common.constants.MongoConstant;
import com.telio.promotion.common.exceptions.EntityNotFoundException;
import com.telio.promotion.core.model.mongo.PromotionAuditEvent;
import com.telio.promotion.core.repository.mongo.PromotionAuditEventRepositoryCustom;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.util.CollectionUtils;

import java.util.Date;
import java.util.List;

public class PromotionAuditEventRepositoryImpl implements PromotionAuditEventRepositoryCustom {

    @Autowired
    private MongoTemplate mongoTemplate;

    @Override
    public PromotionAuditEvent getPromotionAuditEventByOrderId(String orderId) {
        Query query = new Query();
        query.addCriteria(Criteria.where(MongoConstant.PROMOTION_AUDIT_ORDER_ID).is(orderId)
                .and(MongoConstant.PROMOTION_AUDIT_PROMOTION_ACTIVE).is(true));
        List<PromotionAuditEvent> promotionAuditEventList = mongoTemplate.find(query, PromotionAuditEvent.class);
        if(CollectionUtils.isEmpty(promotionAuditEventList)) {
            throw new EntityNotFoundException("No Promotion audit present for order id: "+orderId);
        }
        return promotionAuditEventList.get(0);
    }


    @Override
    public boolean disablePromotionAuditEventByOrderId(String orderId) {
        Query query = new Query();
        query.addCriteria(Criteria.where(MongoConstant.PROMOTION_AUDIT_ORDER_ID).is(orderId)
                .and(MongoConstant.PROMOTION_AUDIT_PROMOTION_ACTIVE).is(true));
        Update update = new Update();
        update.set(MongoConstant.PROMOTION_AUDIT_PROMOTION_ACTIVE, false);
        update.set(MongoConstant.PROMOTION_AUDIT_UPDATED_AT, new Date());
        // It will disable all the present promotion audit for order id.
        // Ideally, there should be one promotion audit active for an order.
        long modifiedCount = mongoTemplate.updateMulti(query, update, PromotionAuditEvent.class).getModifiedCount();
        if(modifiedCount == 0) {
            throw new EntityNotFoundException("No Promotion audit present for order id: "+orderId);
        }
        return true;
    }

    @Override
    public boolean cancelPromotionAuditByOrderId(String orderId) {
        Query query = new Query();
        query.addCriteria(Criteria.where(MongoConstant.PROMOTION_AUDIT_ORDER_ID).is(orderId)
                .and(MongoConstant.PROMOTION_AUDIT_PROMOTION_ACTIVE).is(true));
        Update update = new Update();
        update.set(MongoConstant.PROMOTION_AUDIT_PROMOTION_ACTIVE, false);
        update.set(MongoConstant.PROMOTION_AUDIT_ORDER_CANCELLED, true);
        update.set(MongoConstant.PROMOTION_AUDIT_UPDATED_AT, new Date());
        // It will disable all the present promotion audit for order id.
        // Ideally, there should be one promotion audit active for an order.
        long modifiedCount = mongoTemplate.updateMulti(query, update, PromotionAuditEvent.class).getModifiedCount();
        if(modifiedCount == 0) {
            throw new EntityNotFoundException("No Promotion audit present for order id: "+orderId);
        }
        return true;
    }
}
