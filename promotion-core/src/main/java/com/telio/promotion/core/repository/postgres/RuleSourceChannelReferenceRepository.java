package com.telio.promotion.core.repository.postgres;

import com.telio.promotion.core.model.postgres.RuleSourceChannelReference;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RuleSourceChannelReferenceRepository extends JpaRepository<RuleSourceChannelReference, Long> {
}
