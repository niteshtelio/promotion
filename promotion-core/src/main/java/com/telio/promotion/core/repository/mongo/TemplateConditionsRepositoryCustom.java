package com.telio.promotion.core.repository.mongo;

import com.telio.promotion.core.model.mongo.User;
import com.telio.promotion.dtos.TemplateConditionDto;

public interface TemplateConditionsRepositoryCustom {

    long updateTemplateConditions(String templateConditionId, TemplateConditionDto templateConditionDto, User user);
}
