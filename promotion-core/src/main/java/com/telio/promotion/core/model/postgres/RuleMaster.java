package com.telio.promotion.core.model.postgres;

import com.telio.promotion.common.enums.ApprovalStatus;
import com.telio.promotion.common.enums.RuleType;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Enumerated;
import javax.persistence.EnumType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import java.util.Date;

@Entity
@Table(name = "rule_master")
public class RuleMaster {

    @Id
    @GeneratedValue(
            strategy = GenerationType.AUTO
    )
    @Column(
            name = "rule_id",
            unique = true
    )
    private Long ruleId;

    @NotNull
    @Column(name = "rule_name")
    private String ruleName;

    @Column(name = "rule_description")
    private String ruleDescription;

    @NotNull
    @Column(name = "rule_type")
    @Enumerated(EnumType.STRING)
    private RuleType ruleType;

    @Column(name = "rule_active")
    private Boolean ruleActive;

    @NotNull
    @Column(name = "rule_approved")
    @Enumerated(EnumType.STRING)
    private ApprovalStatus ruleApproved;

    @Column(name = "rule_salience")
    private int ruleSalience;

    @NotNull
    @Column(name = "approved_by")
    private String approvedBy;

    @NotNull
    @Column(name = "modified_by")
    private String modifiedBy;

    @NotNull
    @Column(name = "created_at")
    @CreationTimestamp
    private Date createdAt;

    @NotNull
    @Column(name = "updated_at")
    @UpdateTimestamp
    private Date updatedAt;

    @Column(name = "location")
    private String location;

    @Column(name = "vertical")
    private Date vertical;

    @Column(name = "to_date")
    private Date toDate;

    @Column(name = "coupon")
    private Boolean coupon;

    @Column(name = "coupon_code")
    private String couponCode;

    @Column(name = "avail_per_rule")
    private Integer availPerRule;

    @Column(name = "avail_per_user")
    private Integer availPerUser;

    @Column(name = "avail_per_user_per_day")
    private Integer availPerUserPerDay;

    @Column(name = "rule_template_id")
    private Long ruleTemplateId;

    public RuleMaster() {
        //Default Constructor
    }

    public Long getRuleId() {
        return ruleId;
    }

    public void setRuleId(Long ruleId) {
        this.ruleId = ruleId;
    }

    public String getRuleName() {
        return ruleName;
    }

    public void setRuleName(String ruleName) {
        this.ruleName = ruleName;
    }

    public String getRuleDescription() {
        return ruleDescription;
    }

    public void setRuleDescription(String ruleDescription) {
        this.ruleDescription = ruleDescription;
    }

    public RuleType getRuleType() {
        return ruleType;
    }

    public void setRuleType(RuleType ruleType) {
        this.ruleType = ruleType;
    }

    public Boolean getRuleActive() {
        return ruleActive;
    }

    public void setRuleActive(Boolean ruleActive) {
        this.ruleActive = ruleActive;
    }

    public ApprovalStatus getRuleApproved() {
        return ruleApproved;
    }

    public void setRuleApproved(ApprovalStatus ruleApproved) {
        this.ruleApproved = ruleApproved;
    }

    public int getRuleSalience() {
        return ruleSalience;
    }

    public void setRuleSalience(int ruleSalience) {
        this.ruleSalience = ruleSalience;
    }

    public String getApprovedBy() {
        return approvedBy;
    }

    public void setApprovedBy(String approvedBy) {
        this.approvedBy = approvedBy;
    }

    public String getModifiedBy() {
        return modifiedBy;
    }

    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public Date getVertical() {
        return vertical;
    }

    public void setVertical(Date vertical) {
        this.vertical = vertical;
    }

    public Date getToDate() {
        return toDate;
    }

    public void setToDate(Date toDate) {
        this.toDate = toDate;
    }

    public Boolean getCoupon() {
        return coupon;
    }

    public void setCoupon(Boolean coupon) {
        this.coupon = coupon;
    }

    public String getCouponCode() {
        return couponCode;
    }

    public void setCouponCode(String couponCode) {
        this.couponCode = couponCode;
    }

    public Integer getAvailPerRule() {
        return availPerRule;
    }

    public void setAvailPerRule(Integer availPerRule) {
        this.availPerRule = availPerRule;
    }

    public Integer getAvailPerUser() {
        return availPerUser;
    }

    public void setAvailPerUser(Integer availPerUser) {
        this.availPerUser = availPerUser;
    }

    public Integer getAvailPerUserPerDay() {
        return availPerUserPerDay;
    }

    public void setAvailPerUserPerDay(Integer availPerUserPerDay) {
        this.availPerUserPerDay = availPerUserPerDay;
    }

    public Long getRuleTemplateId() {
        return ruleTemplateId;
    }

    public void setRuleTemplateId(Long ruleTemplateId) {
        this.ruleTemplateId = ruleTemplateId;
    }
}

