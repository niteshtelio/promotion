package com.telio.promotion.core.model.postgres;

import com.telio.promotion.common.enums.ApprovalStatus;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Date;

@Entity
@Table(name = "template_master")
public class TemplateMaster {

    @Id
    @GeneratedValue(
            strategy = GenerationType.IDENTITY
    )
    @Column(
            name = "template_id",
            unique = true
    )
    private Long templateId;

    @NotNull
    @Column(name = "template_name")
    private String templateName;

    @NotNull
    @Column(name = "modified_by")
    private String modifiedBy;

    @Column(name = "created_at")
    @CreationTimestamp
    private Date createdAt;

    @Column(name = "updated_at")
    @UpdateTimestamp
    private Date updatedAt;

    @Column(name = "template_active")
    private Boolean templateActive;

    @Column(name = "template_condition_id")
    private String templateConditionId;

    @NotNull
    @Column(name = "template_approved")
    @Enumerated(EnumType.STRING)
    private ApprovalStatus templateApproved;

    @Column(name = "approved_by")
    private String approvedBy;

    @NotNull
    @Column(name = "template_action_id")
    private String templateActionId;

    @Column(name = "action_id")
    private Integer actionId;

    public TemplateMaster() {
        //Default Constructor
    }

    public Long getTemplateId() {
        return templateId;
    }

    public void setTemplateId(Long templateId) {
        this.templateId = templateId;
    }

    public String getTemplateName() {
        return templateName;
    }

    public void setTemplateName(String templateName) {
        this.templateName = templateName;
    }

    public String getModifiedBy() {
        return modifiedBy;
    }

    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    public Boolean getTemplateActive() {
        return templateActive;
    }

    public void setTemplateActive(Boolean templateActive) {
        this.templateActive = templateActive;
    }

    public String getTemplateConditionId() {
        return templateConditionId;
    }

    public void setTemplateConditionId(String templateConditionId) {
        this.templateConditionId = templateConditionId;
    }

    public ApprovalStatus getTemplateApproved() {
        return templateApproved;
    }

    public void setTemplateApproved(ApprovalStatus templateApproved) {
        this.templateApproved = templateApproved;
    }

    public String getApprovedBy() {
        return approvedBy;
    }

    public void setApprovedBy(String approvedBy) {
        this.approvedBy = approvedBy;
    }

    public String getTemplateActionId() {
        return templateActionId;
    }

    public void setTemplateActionId(String templateActionId) {
        this.templateActionId = templateActionId;
    }

    public Integer getActionId() {
        return actionId;
    }

    public void setActionId(Integer actionId) {
        this.actionId = actionId;
    }
}

