package com.telio.promotion.core.repository.postgres.impl;

import com.querydsl.jpa.impl.JPAQueryFactory;
import com.telio.promotion.common.enums.ApprovalStatus;
import com.telio.promotion.core.model.postgres.QTemplateMaster;
import com.telio.promotion.core.repository.postgres.TemplateMasterRepositoryCustom;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;

public class TemplateMasterRepositoryImpl implements TemplateMasterRepositoryCustom {

    @Autowired
    private JPAQueryFactory jpaQueryFactory;

    @Override
    @Transactional
    public long softDeleteTemplate(Long templateId, String modifiedUserId) {
        QTemplateMaster templateMaster = QTemplateMaster.templateMaster;
        return jpaQueryFactory.update(templateMaster).set(templateMaster.templateActive, false)
                .set(templateMaster.updatedAt, new Date())
                .set(templateMaster.modifiedBy, modifiedUserId).where(templateMaster.templateId.eq(templateId)).execute();
    }

    @Override
    @Transactional
    public long updateTemplateName(Long templateId, String modifiedUserId, String templateName) {
        QTemplateMaster templateMaster = QTemplateMaster.templateMaster;
        return jpaQueryFactory.update(templateMaster)
                .set(templateMaster.templateName, templateName)
                .set(templateMaster.updatedAt, new Date())
                .set(templateMaster.modifiedBy, modifiedUserId).where(templateMaster.templateId.eq(templateId)).execute();
    }

    @Override
    @Transactional
    public long updateTemplateApprovalStatus(Long templateId, String modifiedUserId, ApprovalStatus approvalStatus) {
        QTemplateMaster templateMaster = QTemplateMaster.templateMaster;
        return jpaQueryFactory.update(templateMaster)
                .set(templateMaster.templateApproved, approvalStatus)
                .set(templateMaster.approvedBy, modifiedUserId)
                .set(templateMaster.updatedAt, new Date())
                .set(templateMaster.modifiedBy, modifiedUserId).where(templateMaster.templateId.eq(templateId)).execute();
    }


}
