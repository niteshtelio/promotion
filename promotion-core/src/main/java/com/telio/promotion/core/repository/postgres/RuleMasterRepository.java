package com.telio.promotion.core.repository.postgres;

import com.telio.promotion.core.model.postgres.RuleMaster;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RuleMasterRepository extends JpaRepository<RuleMaster, Long> {
}
