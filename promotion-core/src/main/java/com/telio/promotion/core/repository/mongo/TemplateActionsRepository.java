package com.telio.promotion.core.repository.mongo;

import com.telio.promotion.core.model.mongo.TemplateActions;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;
import java.util.Optional;

@Repository
public interface TemplateActionsRepository extends MongoRepository<TemplateActions, String>, TemplateActionsRepositoryCustom {
    Optional<TemplateActions> findById(String id);
}