package com.telio.promotion.core.repository.postgres;

import com.telio.promotion.core.model.postgres.RuleAttributeMaster;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RuleAttributeMasterRepository extends JpaRepository<RuleAttributeMaster, Long> {
}
