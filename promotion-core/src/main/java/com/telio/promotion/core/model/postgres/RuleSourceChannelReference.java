package com.telio.promotion.core.model.postgres;

import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Date;

@Entity
@Table(name = "rule_source_channel_reference")
public class RuleSourceChannelReference {

    @Id
    @GeneratedValue(
            strategy = GenerationType.AUTO
    )
    @Column(
            name = "rule_source_channel_reference_id",
            unique = true
    )
    private Long ruleSourceChannelReferenceId;

    @Column(name = "rule_id")
    private Long ruleId;

    @NotNull
    @Column(name = "channel")
    private String channel;

    @NotNull
    @Column(name = "created_at")
    @CreationTimestamp
    private Date createdAt;

    public RuleSourceChannelReference() {
        //Default Constructor
    }

    public Long getRuleSourceChannelReferenceId() {
        return ruleSourceChannelReferenceId;
    }

    public void setRuleSourceChannelReferenceId(Long ruleSourceChannelReferenceId) {
        this.ruleSourceChannelReferenceId = ruleSourceChannelReferenceId;
    }

    public Long getRuleId() {
        return ruleId;
    }

    public void setRuleId(Long ruleId) {
        this.ruleId = ruleId;
    }

    public String getChannel() {
        return channel;
    }

    public void setChannel(String channel) {
        this.channel = channel;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }
}
