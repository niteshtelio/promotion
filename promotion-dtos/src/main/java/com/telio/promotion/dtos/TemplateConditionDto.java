package com.telio.promotion.dtos;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.telio.promotion.common.enums.GroupOperators;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class TemplateConditionDto {

    private GroupOperators groupBy;

    @NotNull(message = "Invalid aggregate in request query")
    private List<@Valid List<@Valid AggregatesDto>> aggregates;

    public TemplateConditionDto() {
        //default constructor
    }

    public GroupOperators getGroupBy() {
        return groupBy;
    }

    public void setGroupBy(GroupOperators groupBy) {
        this.groupBy = groupBy;
    }

    public List<List<AggregatesDto>> getAggregates() {
        return aggregates;
    }

    public void setAggregates(List<List<AggregatesDto>> aggregates) {
        this.aggregates = aggregates;
    }
}