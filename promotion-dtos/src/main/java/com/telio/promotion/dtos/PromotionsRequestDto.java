package com.telio.promotion.dtos;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.telio.promotion.common.enums.SkuActions;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class PromotionsRequestDto {

    @NotEmpty(message = "Empty product list")
    private List<@Valid Product> productList;

    @NotBlank(message = "Invalid SKU id")
    private String skuId;

    @NotNull(message = "Invalid SKU action")
    private SkuActions action;

    private String coupon;

    @NotNull(message = "Invalid cart total value")
    private double cartSubTotal;

    @NotNull(message = "Invalid customer info")
    @Valid
    private CustomerInfoDto customerInfo;

    public PromotionsRequestDto() {
        //default constructor
    }

    public List<Product> getProductList() {
        return productList;
    }

    public void setProductList(List<Product> productList) {
        this.productList = productList;
    }

    public String getSkuId() {
        return skuId;
    }

    public void setSkuId(String skuId) {
        this.skuId = skuId;
    }

    public SkuActions getAction() {
        return action;
    }

    public void setAction(SkuActions action) {
        this.action = action;
    }

    public String getCoupon() {
        return coupon;
    }

    public void setCoupon(String coupon) {
        this.coupon = coupon;
    }

    public double getCartSubTotal() {
        return cartSubTotal;
    }

    public void setCartSubTotal(double cartSubTotal) {
        this.cartSubTotal = cartSubTotal;
    }

    public CustomerInfoDto getCustomerInfo() {
        return customerInfo;
    }

    public void setCustomerInfo(CustomerInfoDto customerInfo) {
        this.customerInfo = customerInfo;
    }
}