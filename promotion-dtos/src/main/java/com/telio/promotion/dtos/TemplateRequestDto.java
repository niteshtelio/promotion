package com.telio.promotion.dtos;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.telio.promotion.common.enums.ApprovalStatus;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@JsonIgnoreProperties(ignoreUnknown = true)
public class TemplateRequestDto {

    @NotBlank(message = "Invalid template name")
    private String templateName;
    private boolean templateActive = false;
    private ApprovalStatus approvalStatus = ApprovalStatus.PENDING;
    private TemplateConditionDto templateCondition;
    @NotNull(message = "Invalid template action")
    private TemplateActionDto templateAction;

    public TemplateRequestDto() {
        //default constructor
    }

    public String getTemplateName() {
        return templateName;
    }

    public void setTemplateName(String templateName) {
        this.templateName = templateName;
    }

    public boolean isTemplateActive() {
        return templateActive;
    }

    public void setTemplateActive(boolean templateActive) {
        this.templateActive = templateActive;
    }

    public ApprovalStatus getApprovalStatus() {
        return approvalStatus;
    }

    public void setApprovalStatus(ApprovalStatus approvalStatus) {
        this.approvalStatus = approvalStatus;
    }

    public TemplateConditionDto getTemplateCondition() {
        return templateCondition;
    }

    public void setTemplateCondition(TemplateConditionDto templateCondition) {
        this.templateCondition = templateCondition;
    }

    public TemplateActionDto getTemplateAction() {
        return templateAction;
    }

    public void setTemplateAction(TemplateActionDto templateAction) {
        this.templateAction = templateAction;
    }

    @Override
    public String toString() {
        return "TemplateRequestDto{" +
                "templateName='" + templateName + '\'' +
                ", templateActive=" + templateActive +
                ", approvalStatus=" + approvalStatus +
                ", templateCondition=" + templateCondition +
                ", templateAction=" + templateAction +
                '}';
    }
}
