package com.telio.promotion.dtos;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Product {

    @NotBlank(message = "Invalid SKU id")
    private String skuId;

    @NotNull(message = "Invalid brand id")
    private Integer brandId;

    @NotNull(message = "Invalid category id")
    private Integer categoryId;

    @NotNull(message = "Invalid sub category id")
    private Integer subCategoryId;

    @NotNull(message = "Invalid price per unit")
    private double pricePerUnit;

    @NotNull(message = "Invalid quantity")
    private int quantity;

    @NotNull(message = "Invalid sub total price")
    private double subTotalPrice;

    @NotBlank(message = "Invalid location")
    private String location;

    @NotNull(message = "Invalid vertical code")
    private Integer verticalCode;

    public Product() {
        //Default Constructor
    }

    public String getSkuId() {
        return skuId;
    }

    public void setSkuId(String skuId) {
        this.skuId = skuId;
    }

    public Integer getBrandId() {
        return brandId;
    }

    public void setBrandId(Integer brandId) {
        this.brandId = brandId;
    }

    public Integer getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Integer categoryId) {
        this.categoryId = categoryId;
    }

    public Integer getSubCategoryId() {
        return subCategoryId;
    }

    public void setSubCategoryId(Integer subCategoryId) {
        this.subCategoryId = subCategoryId;
    }

    public double getPricePerUnit() {
        return pricePerUnit;
    }

    public void setPricePerUnit(double pricePerUnit) {
        this.pricePerUnit = pricePerUnit;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public double getSubTotalPrice() {
        return subTotalPrice;
    }

    public void setSubTotalPrice(double subTotalPrice) {
        this.subTotalPrice = subTotalPrice;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public Integer getVerticalCode() {
        return verticalCode;
    }

    public void setVerticalCode(Integer verticalCode) {
        this.verticalCode = verticalCode;
    }
}