package com.telio.promotion.dtos;

import com.telio.promotion.common.enums.ApprovalStatus;

import java.util.Date;

public class TemplateResponseDto {

    private Long templateId;
    private String templateName;
    private boolean templateActive;
    private ApprovalStatus approvalStatus;
    private TemplateConditionDto templateCondition;
    private TemplateActionDto templateAction;
    private UserDto approvedBy;
    private UserDto modifiedBy;
    private Date createdAt;
    private Date updatedAt;

    public TemplateResponseDto() {
        //default constructor
    }

    public Long getTemplateId() {
        return templateId;
    }

    public void setTemplateId(Long templateId) {
        this.templateId = templateId;
    }

    public String getTemplateName() {
        return templateName;
    }

    public void setTemplateName(String templateName) {
        this.templateName = templateName;
    }

    public boolean isTemplateActive() {
        return templateActive;
    }

    public void setTemplateActive(boolean templateActive) {
        this.templateActive = templateActive;
    }

    public ApprovalStatus getApprovalStatus() {
        return approvalStatus;
    }

    public void setApprovalStatus(ApprovalStatus approvalStatus) {
        this.approvalStatus = approvalStatus;
    }

    public TemplateConditionDto getTemplateCondition() {
        return templateCondition;
    }

    public void setTemplateCondition(TemplateConditionDto templateCondition) {
        this.templateCondition = templateCondition;
    }

    public TemplateActionDto getTemplateAction() {
        return templateAction;
    }

    public void setTemplateAction(TemplateActionDto templateAction) {
        this.templateAction = templateAction;
    }

    public UserDto getApprovedBy() {
        return approvedBy;
    }

    public void setApprovedBy(UserDto approvedBy) {
        this.approvedBy = approvedBy;
    }

    public UserDto getModifiedBy() {
        return modifiedBy;
    }

    public void setModifiedBy(UserDto modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }
}