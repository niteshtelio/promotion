package com.telio.promotion.dtos;

import com.telio.promotion.common.enums.ErrorCode;

public class ErrorResponseDto {

    private ErrorCode errorCode;
    private String message;
    private Object errorInfo;

    public ErrorResponseDto() {
        //default constructor
    }

    public ErrorResponseDto(ErrorCode errorCode, String message) {
        this.errorCode = errorCode;
        this.message = message;
    }

    public ErrorResponseDto(ErrorCode errorCode, String message, Object errorInfo) {
        this.errorCode = errorCode;
        this.message = message;
        this.errorInfo = errorInfo;
    }

    public ErrorCode getErrorCode() {
        return this.errorCode;
    }

    public void setErrorCode(ErrorCode errorCode) {
        this.errorCode = errorCode;
    }

    public String getMessage() {
        return this.message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Object getErrorInfo() {
        return errorInfo;
    }

    public void setErrorInfo(Object errorInfo) {
        this.errorInfo = errorInfo;
    }
}
