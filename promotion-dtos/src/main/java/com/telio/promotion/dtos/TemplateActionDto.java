package com.telio.promotion.dtos;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.telio.promotion.common.enums.ActionType;
import com.telio.promotion.common.enums.DiscountType;

import javax.validation.constraints.NotNull;
import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class TemplateActionDto {

    @NotNull(message = "Invalid action id")
    private Integer actionId;

    @NotNull(message = "Invalid action type")
    private ActionType actionType;

    @NotNull(message = "Invalid discount type")
    private DiscountType discountType;
    private double discountPercentage;
    private double discountAmount;
    private List<String> discountOnSkus;
    private Integer maximumQuantityDiscount;
    private boolean freeShipping = false;

    public TemplateActionDto() {
        //default constructor
    }

    public Integer getActionId() {
        return actionId;
    }

    public void setActionId(Integer actionId) {
        this.actionId = actionId;
    }

    public ActionType getActionType() {
        return actionType;
    }

    public void setActionType(ActionType actionType) {
        this.actionType = actionType;
    }

    public DiscountType getDiscountType() {
        return discountType;
    }

    public void setDiscountType(DiscountType discountType) {
        this.discountType = discountType;
    }

    public double getDiscountPercentage() {
        return discountPercentage;
    }

    public void setDiscountPercentage(double discountPercentage) {
        this.discountPercentage = discountPercentage;
    }

    public double getDiscountAmount() {
        return discountAmount;
    }

    public void setDiscountAmount(double discountAmount) {
        this.discountAmount = discountAmount;
    }

    public List<String> getDiscountOnSkus() {
        return discountOnSkus;
    }

    public void setDiscountOnSkus(List<String> discountOnSkus) {
        this.discountOnSkus = discountOnSkus;
    }

    public Integer getMaximumQuantityDiscount() {
        return maximumQuantityDiscount;
    }

    public void setMaximumQuantityDiscount(Integer maximumQuantityDiscount) {
        this.maximumQuantityDiscount = maximumQuantityDiscount;
    }

    public boolean isFreeShipping() {
        return freeShipping;
    }

    public void setFreeShipping(boolean freeShipping) {
        this.freeShipping = freeShipping;
    }
}