package com.telio.promotion.dtos;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.telio.promotion.common.enums.ApprovalStatus;
import com.telio.promotion.common.enums.RuleType;

import javax.validation.constraints.NotNull;
import java.util.Date;
import java.util.Map;

@JsonIgnoreProperties(ignoreUnknown = true)
public class RuleUpdateDto {

    @NotNull(message = "Invalid rule id")
    private Long ruleId;
    private String ruleName;
    private String ruleDescription;
    private RuleType ruleType;
    private boolean ruleActive;
    private ApprovalStatus ruleApproved;
    private Integer ruleSalience;
    private String location;
    private String vertical;
    private Date fromDate;
    private Date toDate;
    private boolean coupon;
    private String couponCode;
    private Integer availPerRule;
    private Integer availPerUser;
    private Integer availPerUserPerDay;
    private Long ruleTemplateId;
    private Map<String, String> ruleAttributesMap;

    public RuleUpdateDto() {
        //default constructor
    }

    public Long getRuleId() {
        return ruleId;
    }

    public void setRuleId(Long ruleId) {
        this.ruleId = ruleId;
    }

    public String getRuleName() {
        return ruleName;
    }

    public void setRuleName(String ruleName) {
        this.ruleName = ruleName;
    }

    public String getRuleDescription() {
        return ruleDescription;
    }

    public void setRuleDescription(String ruleDescription) {
        this.ruleDescription = ruleDescription;
    }

    public RuleType getRuleType() {
        return ruleType;
    }

    public void setRuleType(RuleType ruleType) {
        this.ruleType = ruleType;
    }

    public boolean isRuleActive() {
        return ruleActive;
    }

    public void setRuleActive(boolean ruleActive) {
        this.ruleActive = ruleActive;
    }

    public ApprovalStatus getRuleApproved() {
        return ruleApproved;
    }

    public void setRuleApproved(ApprovalStatus ruleApproved) {
        this.ruleApproved = ruleApproved;
    }

    public Integer getRuleSalience() {
        return ruleSalience;
    }

    public void setRuleSalience(Integer ruleSalience) {
        this.ruleSalience = ruleSalience;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getVertical() {
        return vertical;
    }

    public void setVertical(String vertical) {
        this.vertical = vertical;
    }

    public Date getFromDate() {
        return fromDate;
    }

    public void setFromDate(Date fromDate) {
        this.fromDate = fromDate;
    }

    public Date getToDate() {
        return toDate;
    }

    public void setToDate(Date toDate) {
        this.toDate = toDate;
    }

    public boolean isCoupon() {
        return coupon;
    }

    public void setCoupon(boolean coupon) {
        this.coupon = coupon;
    }

    public String getCouponCode() {
        return couponCode;
    }

    public void setCouponCode(String couponCode) {
        this.couponCode = couponCode;
    }

    public Integer getAvailPerRule() {
        return availPerRule;
    }

    public void setAvailPerRule(Integer availPerRule) {
        this.availPerRule = availPerRule;
    }

    public Integer getAvailPerUser() {
        return availPerUser;
    }

    public void setAvailPerUser(Integer availPerUser) {
        this.availPerUser = availPerUser;
    }

    public Integer getAvailPerUserPerDay() {
        return availPerUserPerDay;
    }

    public void setAvailPerUserPerDay(Integer availPerUserPerDay) {
        this.availPerUserPerDay = availPerUserPerDay;
    }

    public Long getRuleTemplateId() {
        return ruleTemplateId;
    }

    public void setRuleTemplateId(Long ruleTemplateId) {
        this.ruleTemplateId = ruleTemplateId;
    }

    public Map<String, String> getRuleAttributesMap() {
        return ruleAttributesMap;
    }

    public void setRuleAttributesMap(Map<String, String> ruleAttributesMap) {
        this.ruleAttributesMap = ruleAttributesMap;
    }
}