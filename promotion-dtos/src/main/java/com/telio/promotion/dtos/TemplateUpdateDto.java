package com.telio.promotion.dtos;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.telio.promotion.common.enums.ApprovalStatus;

import javax.validation.constraints.NotNull;

@JsonIgnoreProperties(ignoreUnknown = true)
public class TemplateUpdateDto {

    @NotNull(message = "Invalid template id")
    private Long templateId;
    private String templateName;
    private boolean templateActive;
    private ApprovalStatus approvalStatus;
    private TemplateConditionDto templateCondition;
    private TemplateActionDto templateAction;

    public TemplateUpdateDto() {
        //default constructor
    }

    public Long getTemplateId() {
        return templateId;
    }

    public void setTemplateId(Long templateId) {
        this.templateId = templateId;
    }

    public String getTemplateName() {
        return templateName;
    }

    public void setTemplateName(String templateName) {
        this.templateName = templateName;
    }

    public boolean isTemplateActive() {
        return templateActive;
    }

    public void setTemplateActive(boolean templateActive) {
        this.templateActive = templateActive;
    }

    public ApprovalStatus getApprovalStatus() {
        return approvalStatus;
    }

    public void setApprovalStatus(ApprovalStatus approvalStatus) {
        this.approvalStatus = approvalStatus;
    }

    public TemplateConditionDto getTemplateCondition() {
        return templateCondition;
    }

    public void setTemplateCondition(TemplateConditionDto templateCondition) {
        this.templateCondition = templateCondition;
    }

    public TemplateActionDto getTemplateAction() {
        return templateAction;
    }

    public void setTemplateAction(TemplateActionDto templateAction) {
        this.templateAction = templateAction;
    }
}