package com.telio.promotion.dtos.audit;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.telio.promotion.common.enums.DiscountType;
import com.telio.promotion.dtos.Product;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.Date;
import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class PromotionAuditEventRequestDto {

    @JsonProperty("order-id")
    @NotBlank(message = "Invalid order id")
    private String orderId;

    @JsonProperty("sku")
    @NotEmpty(message = "Empty SKU list")
    private List<@Valid Product> sku;

    @JsonProperty("promotion-sku")
    @NotEmpty(message = "Empty Promotion SKU")
    private List<@Valid Product> promotionsSku;

    @JsonProperty("discount-type")
    @NotNull(message = "Discount Type is null")
    private DiscountType discountType;

    @JsonProperty("discount")
    @NotNull(message = "Discount is null")
    private Double discount;

    @JsonProperty("rule-id")
    @NotNull(message = "Rule Id is null")
    private Long ruleId;

    @JsonProperty("applied-at")
    @NotNull(message = "Promotion applied timestamp is null")
    private Date appliedAt;

    @JsonProperty("promotion-coupon-code")
    private String promotionCouponCode;

    @JsonProperty("entity-applied-on")
    @NotBlank(message = "Invalid entity applied for Promotion")
    private String entityAppliedOn;

    @JsonProperty("cart-total-price")
    @NotNull(message = "Cart total price is null")
    private Double cartTotalPrice;

    @JsonProperty("channel")
    @NotBlank(message = "Invalid channel")
    private String channel;

    public PromotionAuditEventRequestDto() {
        //Default Constructor
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public List<Product> getSku() {
        return sku;
    }

    public void setSku(List<Product> sku) {
        this.sku = sku;
    }

    public List<Product> getPromotionsSku() {
        return promotionsSku;
    }

    public void setPromotionsSku(List<Product> promotionsSku) {
        this.promotionsSku = promotionsSku;
    }

    public DiscountType getDiscountType() {
        return discountType;
    }

    public void setDiscountType(DiscountType discountType) {
        this.discountType = discountType;
    }

    public Double getDiscount() {
        return discount;
    }

    public void setDiscount(Double discount) {
        this.discount = discount;
    }

    public Long getRuleId() {
        return ruleId;
    }

    public void setRuleId(Long ruleId) {
        this.ruleId = ruleId;
    }

    public Date getAppliedAt() {
        return appliedAt;
    }

    public void setAppliedAt(Date appliedAt) {
        this.appliedAt = appliedAt;
    }

    public String getPromotionCouponCode() {
        return promotionCouponCode;
    }

    public void setPromotionCouponCode(String promotionCouponCode) {
        this.promotionCouponCode = promotionCouponCode;
    }

    public String getEntityAppliedOn() {
        return entityAppliedOn;
    }

    public void setEntityAppliedOn(String entityAppliedOn) {
        this.entityAppliedOn = entityAppliedOn;
    }

    public Double getCartTotalPrice() {
        return cartTotalPrice;
    }

    public void setCartTotalPrice(Double cartTotalPrice) {
        this.cartTotalPrice = cartTotalPrice;
    }

    public String getChannel() {
        return channel;
    }

    public void setChannel(String channel) {
        this.channel = channel;
    }
}

