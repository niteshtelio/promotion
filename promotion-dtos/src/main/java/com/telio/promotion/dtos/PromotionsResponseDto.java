package com.telio.promotion.dtos;

import com.telio.promotion.common.enums.DiscountType;
import com.telio.promotion.common.enums.PromotionType;
import com.telio.promotion.common.enums.SkuActions;

import java.util.Map;

public class PromotionsResponseDto {

    private boolean promotionApplied;
    private PromotionType promotionType;
    private String promotionAvailInfo;
    private SkuActions action;
    private Map<String, Integer> skuIdQuantityMap;
    private double discount;
    private DiscountType discountType;
    private double cartSubTotal;

    public PromotionsResponseDto() {
        //default constructor
    }

    public boolean isPromotionApplied() {
        return promotionApplied;
    }

    public void setPromotionApplied(boolean promotionApplied) {
        this.promotionApplied = promotionApplied;
    }

    public PromotionType getPromotionType() {
        return promotionType;
    }

    public void setPromotionType(PromotionType promotionType) {
        this.promotionType = promotionType;
    }

    public String getPromotionAvailInfo() {
        return promotionAvailInfo;
    }

    public void setPromotionAvailInfo(String promotionAvailInfo) {
        this.promotionAvailInfo = promotionAvailInfo;
    }

    public SkuActions getAction() {
        return action;
    }

    public void setAction(SkuActions action) {
        this.action = action;
    }

    public Map<String, Integer> getSkuIdQuantityMap() {
        return skuIdQuantityMap;
    }

    public void setSkuIdQuantityMap(Map<String, Integer> skuIdQuantityMap) {
        this.skuIdQuantityMap = skuIdQuantityMap;
    }

    public DiscountType getDiscountType() {
        return discountType;
    }

    public void setDiscountType(DiscountType discountType) {
        this.discountType = discountType;
    }

    public double getCartSubTotal() {
        return cartSubTotal;
    }

    public void setCartSubTotal(double cartSubTotal) {
        this.cartSubTotal = cartSubTotal;
    }

    public double getDiscount() {
        return discount;
    }

    public void setDiscount(double discount) {
        this.discount = discount;
    }
}