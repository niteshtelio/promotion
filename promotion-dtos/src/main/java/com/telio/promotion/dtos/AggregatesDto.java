package com.telio.promotion.dtos;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.telio.promotion.common.enums.AggregateFields;
import com.telio.promotion.common.enums.GroupOperators;
import com.telio.promotion.common.enums.PredicateOperators;

import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class AggregatesDto {

    @NotNull(message = "Invalid aggregation on entity")
    private AggregateFields aggregationOn;

    @NotEmpty(message = "Empty aggregate fields list")
    private List<@Valid FieldsDto> fields;

    @NotNull(message = "Invalid predicate operator")
    private PredicateOperators predicateOperator;

    @NotEmpty(message = "Invalid predicate value")
    private String predicateValue;

    private GroupOperators aggregateGroupBy;

    public AggregatesDto() {
        //default constructor
    }

    public AggregateFields getAggregationOn() {
        return aggregationOn;
    }

    public void setAggregationOn(AggregateFields aggregationOn) {
        this.aggregationOn = aggregationOn;
    }

    public List<FieldsDto> getFields() {
        return fields;
    }

    public void setFields(List<FieldsDto> fields) {
        this.fields = fields;
    }

    public PredicateOperators getPredicateOperator() {
        return predicateOperator;
    }

    public void setPredicateOperator(PredicateOperators predicateOperator) {
        this.predicateOperator = predicateOperator;
    }

    public String getPredicateValue() {
        return predicateValue;
    }

    public void setPredicateValue(String predicateValue) {
        this.predicateValue = predicateValue;
    }

    public GroupOperators getAggregateGroupBy() {
        return aggregateGroupBy;
    }

    public void setAggregateGroupBy(GroupOperators aggregateGroupBy) {
        this.aggregateGroupBy = aggregateGroupBy;
    }
}