package com.telio.promotion.dtos;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.telio.promotion.common.enums.ConditionFields;
import com.telio.promotion.common.enums.FieldOperators;
import com.telio.promotion.common.enums.GroupOperators;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class FieldsDto {

    @NotNull(message = "Invalid condition field")
    private ConditionFields field;

    @NotNull(message = "Invalid field operator")
    private FieldOperators fieldOperator;

    @NotNull(message = "Invalid field value")
    private String fieldValue;

    private GroupOperators fieldGroupBy;

    private List<@Valid FieldsDto> subFields;

    public FieldsDto() {
        //default constructor
    }

    public ConditionFields getField() {
        return field;
    }

    public void setField(ConditionFields field) {
        this.field = field;
    }

    public FieldOperators getFieldOperator() {
        return fieldOperator;
    }

    public void setFieldOperator(FieldOperators fieldOperator) {
        this.fieldOperator = fieldOperator;
    }

    public String getFieldValue() {
        return fieldValue;
    }

    public void setFieldValue(String fieldValue) {
        this.fieldValue = fieldValue;
    }

    public GroupOperators getFieldGroupBy() {
        return fieldGroupBy;
    }

    public void setFieldGroupBy(GroupOperators fieldGroupBy) {
        this.fieldGroupBy = fieldGroupBy;
    }

    public List<FieldsDto> getSubFields() {
        return subFields;
    }

    public void setSubFields(List<FieldsDto> subFields) {
        this.subFields = subFields;
    }
}